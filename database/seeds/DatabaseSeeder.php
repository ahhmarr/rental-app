<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(ProfileSeeder::class);
         $this->call(HomeTypeSeeder::class);
         $this->call(AmenityTypeSeeder::class);
         $this->call(AmenitySeeder::class);
         $this->call(HomeSafetiesTypesSeeder::class);
         $this->call(RoomTypesSeeder::class);
         $this->call(PropertySeeder::class);
         $this->call(PropertyAddressSeeder::class);
         $this->call(PropertyAmenitiesSeeder::class);
         $this->call(PropertySafetiesSeeder::class);
         $this->call(PropertyHeadingSeeder::class);
         $this->call(PropertyPhotosSeeder::class);
         $this->call(PropertyBookingSeeder::class);
    }
}

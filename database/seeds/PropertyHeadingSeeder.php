<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\RentalListHeading;
class PropertyHeadingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $headings = [
            ['rental_id'=>'1','heading'=>'Well Furnished Room','summary'=>'Lorem ipsum dolor sit amet','activity'=>'Lorem ipsum dolor sit amet'],
            ['rental_id'=>'2','heading'=>'Luxury Lofts, Hot Locations','summary'=>'Lorem ipsum dolor sit amet','activity'=>'Lorem ipsum dolor sit amet'],
            ['rental_id'=>'3','heading'=>'Luxury Lofts, Hot Locations','summary'=>'Lorem ipsum dolor sit amet','activity'=>'Lorem ipsum dolor sit amet'],
            ['rental_id'=>'4','heading'=>'Stylish Chic, Best of west village','summary'=>'Lorem ipsum dolor sit amet, consectetur adipiscing','activity'=>'Lorem ipsum dolor sit amet'],
            ['rental_id'=>'5','heading'=>'NYC waterfront Artist Studio','summary'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit,','activity'=>'Lorem ipsum dolor sit amet'],
            ['rental_id'=>'6','heading'=>'Luxury Apartment, Theater District','summary'=>'Lorem ipsum dolor sit amet, consectetur adipiscing','activity'=>'Lorem ipsum dolor sit amet'],
        ];

        foreach($headings as $heading){
            \App\PropertyHeading::create($heading);
        }



    }
}

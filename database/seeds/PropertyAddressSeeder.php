<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\RentalListAddress;
class PropertyAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $address=[
            ['rental_id'=>'1','Search_address'=>'52-A Elgin Road','street_number'=>'52-A','address'=>'Elgin Road','city'=>'Lucknow','state'=>'Uttar Pradesh','country'=>'india','zip'=>'211001','phone_number'=>'+91888888','latitude'=>'25.4358011','longitude'=>'81.84631100000001'],
            ['rental_id'=>'2','Search_address'=>'42/6A Tagore Town','street_number'=>'426/6A','address'=>'Tagore town','city'=>'Allahabad','state'=>'Uttar Pradesh','country'=>'india','zip'=>'211005','phone_number'=>'+91888888','latitude'=>'25.4358011','longitude'=>'81.84631100000001'],
            ['rental_id'=>'3','Search_address'=>'245-V Vasant Vihar','street_number'=>'4245-V','address'=>'Vasant Vihar','city'=>'Delhi','state'=>'Delhi','country'=>'india','zip'=>'211045','phone_number'=>'+91888888','latitude'=>'25.4358011','longitude'=>'81.84631100000001'],
            ['rental_id'=>'4','Search_address'=>'54-G North Street','street_number'=>'654-G','address'=>'North Street','city'=>'Mumbai','state'=>'Maharasta','country'=>'india','zip'=>'211545','phone_number'=>'+91888888','latitude'=>'25.4358011','longitude'=>'81.84631100000001'],
            ['rental_id'=>'5','Search_address'=>'44-G North Street','street_number'=>'654-G','address'=>'North Street','city'=>'Mumbai','state'=>'Maharasta','country'=>'india','zip'=>'211545','phone_number'=>'+91888888','latitude'=>'25.4358011','longitude'=>'81.84631100000001'],
            ['rental_id'=>'6','Search_address'=>'89-C South Street','street_number'=>'98-C','address'=>'South Street','city'=>'Nasik','state'=>'Maharasta','country'=>'india','zip'=>'411545','phone_number'=>'+91888888','latitude'=>'25.4358011','longitude'=>'81.84631100000001'],
        ];
        foreach($address as $add){
            \App\PropertyAddress::create($add);
        }



    }
}

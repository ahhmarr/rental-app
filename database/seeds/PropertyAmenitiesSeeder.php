<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\RentalListAmenity;
class PropertyAmenitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $amenities = [
            ['rental_id'=>'1','amenity_id'=>'1'],
            ['rental_id'=>'1','amenity_id'=>'2'],
            ['rental_id'=>'1','amenity_id'=>'3'],
            ['rental_id'=>'1','amenity_id'=>'4'],
            ['rental_id'=>'1','amenity_id'=>'5'],
            ['rental_id'=>'1','amenity_id'=>'9'],
            ['rental_id'=>'1','amenity_id'=>'10'],
            ['rental_id'=>'1','amenity_id'=>'11'],
            ['rental_id'=>'1','amenity_id'=>'15'],
            ['rental_id'=>'1','amenity_id'=>'17'],
            ['rental_id'=>'1','amenity_id'=>'18'],
            ['rental_id'=>'1','amenity_id'=>'19'],
            ['rental_id'=>'1','amenity_id'=>'20'],

            ['rental_id'=>'2','amenity_id'=>'1'],
            ['rental_id'=>'2','amenity_id'=>'2'],
            ['rental_id'=>'2','amenity_id'=>'3'],
            ['rental_id'=>'2','amenity_id'=>'4'],
            ['rental_id'=>'2','amenity_id'=>'5'],
            ['rental_id'=>'2','amenity_id'=>'9'],
            ['rental_id'=>'2','amenity_id'=>'10'],
            ['rental_id'=>'2','amenity_id'=>'11'],
            ['rental_id'=>'2','amenity_id'=>'15'],
            ['rental_id'=>'2','amenity_id'=>'17'],
            ['rental_id'=>'2','amenity_id'=>'18'],
            ['rental_id'=>'2','amenity_id'=>'19'],
            ['rental_id'=>'2','amenity_id'=>'20'],

            ['rental_id'=>'3','amenity_id'=>'1'],
            ['rental_id'=>'3','amenity_id'=>'2'],
            ['rental_id'=>'3','amenity_id'=>'3'],
            ['rental_id'=>'3','amenity_id'=>'4'],
            ['rental_id'=>'3','amenity_id'=>'5'],
            ['rental_id'=>'3','amenity_id'=>'9'],
            ['rental_id'=>'3','amenity_id'=>'10'],
            ['rental_id'=>'3','amenity_id'=>'11'],
            ['rental_id'=>'3','amenity_id'=>'15'],
            ['rental_id'=>'3','amenity_id'=>'17'],
            ['rental_id'=>'3','amenity_id'=>'18'],
            ['rental_id'=>'3','amenity_id'=>'19'],
            ['rental_id'=>'3','amenity_id'=>'20'],

            ['rental_id'=>'4','amenity_id'=>'1'],
            ['rental_id'=>'4','amenity_id'=>'2'],
            ['rental_id'=>'4','amenity_id'=>'3'],
            ['rental_id'=>'4','amenity_id'=>'4'],
            ['rental_id'=>'4','amenity_id'=>'5'],
            ['rental_id'=>'4','amenity_id'=>'9'],
            ['rental_id'=>'4','amenity_id'=>'10'],
            ['rental_id'=>'4','amenity_id'=>'11'],
            ['rental_id'=>'4','amenity_id'=>'15'],
            ['rental_id'=>'4','amenity_id'=>'17'],
            ['rental_id'=>'4','amenity_id'=>'18'],
            ['rental_id'=>'4','amenity_id'=>'19'],
            ['rental_id'=>'4','amenity_id'=>'20'],

            ['rental_id'=>'5','amenity_id'=>'1'],
            ['rental_id'=>'5','amenity_id'=>'2'],
            ['rental_id'=>'5','amenity_id'=>'3'],
            ['rental_id'=>'5','amenity_id'=>'4'],
            ['rental_id'=>'5','amenity_id'=>'5'],
            ['rental_id'=>'5','amenity_id'=>'9'],
            ['rental_id'=>'5','amenity_id'=>'10'],
            ['rental_id'=>'5','amenity_id'=>'11'],
            ['rental_id'=>'5','amenity_id'=>'15'],
            ['rental_id'=>'5','amenity_id'=>'17'],
            ['rental_id'=>'5','amenity_id'=>'18'],
            ['rental_id'=>'5','amenity_id'=>'19'],
            ['rental_id'=>'5','amenity_id'=>'20'],

            ['rental_id'=>'6','amenity_id'=>'1'],
            ['rental_id'=>'6','amenity_id'=>'2'],
            ['rental_id'=>'6','amenity_id'=>'3'],
            ['rental_id'=>'6','amenity_id'=>'4'],
            ['rental_id'=>'6','amenity_id'=>'5'],
            ['rental_id'=>'6','amenity_id'=>'9'],
            ['rental_id'=>'6','amenity_id'=>'10'],
            ['rental_id'=>'6','amenity_id'=>'11'],
            ['rental_id'=>'6','amenity_id'=>'15'],
            ['rental_id'=>'6','amenity_id'=>'17'],
            ['rental_id'=>'6','amenity_id'=>'18'],
            ['rental_id'=>'6','amenity_id'=>'19'],
            ['rental_id'=>'6','amenity_id'=>'20'],

        ];
        foreach($amenities as $amenity)
        {
            \App\PropertyAmenity::create($amenity);
        }



    }
}

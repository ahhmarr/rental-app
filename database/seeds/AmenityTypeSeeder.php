<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\AmenitiesType;
class AmenityTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $data=[
            'Common Amenities',
            'Additional Amenities',
            'Special Amenities',
            'House Rules'
        ];
        foreach($data as $d){
            AmenitiesType::create([
                'amenity_type'  => $d,

            ]);
        }


    }
}

<?php

use Illuminate\Database\Seeder;

class PropertyBookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['rental_id'=>'1','check_in_time'=>'03:00 PM','check_out_time'=>'12:00 PM','security_deposit'=>'10','cancel_policy'=>'1'],
            ['rental_id'=>'2','check_in_time'=>'03:00 PM','check_out_time'=>'12:00 PM','security_deposit'=>'10','cancel_policy'=>'2'],
            ['rental_id'=>'3','check_in_time'=>'03:00 PM','check_out_time'=>'12:00 PM','security_deposit'=>'10','cancel_policy'=>'1'],
            ['rental_id'=>'4','check_in_time'=>'03:00 PM','check_out_time'=>'12:00 PM','security_deposit'=>'10','cancel_policy'=>'3'],
            ['rental_id'=>'5','check_in_time'=>'03:00 PM','check_out_time'=>'12:00 PM','security_deposit'=>'10','cancel_policy'=>'2'],
            ['rental_id'=>'6','check_in_time'=>'03:00 PM','check_out_time'=>'12:00 PM','security_deposit'=>'10','cancel_policy'=>'2'],
        ];
        foreach ($data as $d) {
            \App\PropertyBooking::create($d);
        }
    }
}

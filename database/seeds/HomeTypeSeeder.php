<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\HomeType;
class HomeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $data=[
            'apartment',
            'villa',
            'house'
        ];
            foreach($data as $d){
                HomeType::create([
                    'home_type'  => $d,

                ]);
        }


    }
}

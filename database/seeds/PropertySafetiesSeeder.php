<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\RentalListSafety;
class PropertySafetiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $safeties = [
            ['rental_id'=>'1','safety_id'=>'1'],
            ['rental_id'=>'1','safety_id'=>'2'],
            ['rental_id'=>'1','safety_id'=>'3'],

            ['rental_id'=>'2','safety_id'=>'1'],
            ['rental_id'=>'2','safety_id'=>'2'],
            ['rental_id'=>'2','safety_id'=>'3'],

            ['rental_id'=>'3','safety_id'=>'1'],
            ['rental_id'=>'3','safety_id'=>'2'],
            ['rental_id'=>'3','safety_id'=>'3'],

            ['rental_id'=>'4','safety_id'=>'1'],
            ['rental_id'=>'4','safety_id'=>'2'],
            ['rental_id'=>'4','safety_id'=>'3'],

            ['rental_id'=>'5','safety_id'=>'1'],
            ['rental_id'=>'5','safety_id'=>'2'],
            ['rental_id'=>'5','safety_id'=>'3'],

            ['rental_id'=>'6','safety_id'=>'1'],
            ['rental_id'=>'6','safety_id'=>'2'],
            ['rental_id'=>'6','safety_id'=>'3'],
        ];
        foreach($safeties as $safety){
            \App\PropertySafety::create($safety);
        }



    }
}

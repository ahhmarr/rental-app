<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Amenity;
class AmenitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $data=[
            ['amenity_type_id'=>'1', 'amenity'=>'Essentials'],
            ['amenity_type_id'=>'1', 'amenity'=>'Tv'],
            ['amenity_type_id'=>'1', 'amenity'=>'Cable Tv'],
            ['amenity_type_id'=>'1', 'amenity'=>'Air Conditioning'],
            ['amenity_type_id'=>'1', 'amenity'=>'Air Conditioning'],
            ['amenity_type_id'=>'1', 'amenity'=>'Kitchen'],
            ['amenity_type_id'=>'1', 'amenity'=>'Internet'],
            ['amenity_type_id'=>'1', 'amenity'=>'24-Hour Check-in'],
            ['amenity_type_id'=>'2', 'amenity'=>'Hot Tub'],
            ['amenity_type_id'=>'2', 'amenity'=>'Washer'],
            ['amenity_type_id'=>'2', 'amenity'=>'Pool'],
            ['amenity_type_id'=>'2', 'amenity'=>'Dryer'],
            ['amenity_type_id'=>'2', 'amenity'=>'BreakFast'],
            ['amenity_type_id'=>'2', 'amenity'=>'Elevator'],
            ['amenity_type_id'=>'3', 'amenity'=>'Family/kid friendly'],
            ['amenity_type_id'=>'3', 'amenity'=>'Smoking Allowed'],
            ['amenity_type_id'=>'3', 'amenity'=>'Pets Allowed'],
            ['amenity_type_id'=>'4', 'amenity'=>'No Smoking'],
            ['amenity_type_id'=>'4', 'amenity'=>'No Drinking'],
            ['amenity_type_id'=>'4', 'amenity'=>'No Pet Allowed'],
            ['amenity_type_id'=>'4', 'amenity'=>'Children Not Welcome'],
        ];
        foreach($data as $d)
            Amenity::create($d);
        }



}

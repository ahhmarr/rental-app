<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['user_id'=>'1','street'=>'598-A','address'=>'M G Road','city'=>'Kanpur','state'=>'Uttar Pradesh',
                'country'=>'india','zip'=>'211001','phone_number'=>'9876543210',
                'about'=>'Lorem ipsum dolor sit amet,
                 consectetur adipisicing elit. Animi corporis distinctio libero natus officia perferendis quod, rerum
                  tenetur. Cumque, sint, voluptatum. Adipisci animi autem commodi, dolorum eum nobis quaerat voluptatum.',
            'work'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi corporis distinctio libero natus
             officia perferendis quod, rerum tenetur. Cumque, sint, voluptatum. Adipisci animi autem commodi, dolorum
              eum nobis quaerat voluptatum.',
                'hobbies'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi corporis distinctio libero
                 natus officia perferendis quod, rerum tenetur. Cumque, sint, voluptatum. Adipisci animi autem
                  commodi, dolorum eum nobis quaerat voluptatum.',
            'profile_pic'=>'http://www.officialpsds.com/images/thumbs/Super-Mario-Pixel-psd37077.png'],

            ['user_id'=>'2','street'=>'426/6A','address'=>'Tagore Town','city'=>'Allahabad','state'=>'Uttar Pradesh',
                'country'=>'india','zip'=>'211001','phone_number'=>'9876543210',
                'about'=>'Lorem ipsum dolor sit amet,
                 consectetur adipisicing elit. Animi corporis distinctio libero natus officia perferendis quod, rerum
                  tenetur. Cumque, sint, voluptatum. Adipisci animi autem commodi, dolorum eum nobis quaerat voluptatum.',
                'work'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi corporis distinctio libero natus
             officia perferendis quod, rerum tenetur. Cumque, sint, voluptatum. Adipisci animi autem commodi, dolorum
              eum nobis quaerat voluptatum.',
                'hobbies'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi corporis distinctio libero
                 natus officia perferendis quod, rerum tenetur. Cumque, sint, voluptatum. Adipisci animi autem
                  commodi, dolorum eum nobis quaerat voluptatum.',
                'profile_pic'=>'https://minecraftpixelarttemplates.files.wordpress.com/2013/05/sonic.png'],

            ['user_id'=>'3','street'=>'123-D','address'=>'Civil Lines','city'=>'Allahabad','state'=>'Uttar Pradesh',
                'country'=>'india','zip'=>'211001','phone_number'=>'9876543210',
                'about'=>'Lorem ipsum dolor sit amet,
                 consectetur adipisicing elit. Animi corporis distinctio libero natus officia perferendis quod, rerum
                  tenetur. Cumque, sint, voluptatum. Adipisci animi autem commodi, dolorum eum nobis quaerat voluptatum.',
                'work'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi corporis distinctio libero natus
             officia perferendis quod, rerum tenetur. Cumque, sint, voluptatum. Adipisci animi autem commodi, dolorum
              eum nobis quaerat voluptatum.',
                'hobbies'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi corporis distinctio libero
                 natus officia perferendis quod, rerum tenetur. Cumque, sint, voluptatum. Adipisci animi autem
                  commodi, dolorum eum nobis quaerat voluptatum.',
                'profile_pic'=>'http://4.bp.blogspot.com/-OvZ07yzLeYQ/UXEIgx48m9I/AAAAAAAAGN0/EEBJXQUSJKg/s400/batman-sign-pixel-art.png'],
        ];

        foreach ($data as $d) {
            \App\Profile::create($d);
        }
    }
}

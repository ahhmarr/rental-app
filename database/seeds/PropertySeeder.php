<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\RentalListRental;
class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $rentals = [
            ['user_id'=>'1','home_type'=>'1','room_type'=>'2','bedroom'=>'3','bathroom'=>'3','bed'=>'4','accommodate'=>'6','rate'=>'200','rate_extra_person'=>'2','rate_cleaning'=>'1','weekly_discount'=>'5','monthly_discount'=>'5','currency'=>'$'],
            ['user_id'=>'1','home_type'=>'2','room_type'=>'1','bedroom'=>'4','bathroom'=>'4','bed'=>'5','accommodate'=>'7','rate'=>'500','rate_extra_person'=>'2','rate_cleaning'=>'1','weekly_discount'=>'5','monthly_discount'=>'5','currency'=>'$'],
            ['user_id'=>'1','home_type'=>'3','room_type'=>'3','bedroom'=>'2','bathroom'=>'1','bed'=>'2','accommodate'=>'2','rate'=>'400','rate_extra_person'=>'2','rate_cleaning'=>'1','weekly_discount'=>'5','monthly_discount'=>'5','currency'=>'₹'],
            ['user_id'=>'1','home_type'=>'2','room_type'=>'1','bedroom'=>'10','bathroom'=>'5','bed'=>'5','accommodate'=>'5','rate'=>'800','rate_extra_person'=>'2','rate_cleaning'=>'1','weekly_discount'=>'5','monthly_discount'=>'5','currency'=>'₹'],
            ['user_id'=>'1','home_type'=>'2','room_type'=>'1','bedroom'=>'10','bathroom'=>'5','bed'=>'5','accommodate'=>'5','rate'=>'200','rate_extra_person'=>'2','rate_cleaning'=>'1','weekly_discount'=>'5','monthly_discount'=>'5','currency'=>'€'],
            ['user_id'=>'1','home_type'=>'2','room_type'=>'1','bedroom'=>'5','bathroom'=>'10','bed'=>'10','accommodate'=>'10','rate'=>'300','rate_extra_person'=>'2','rate_cleaning'=>'1','weekly_discount'=>'5','monthly_discount'=>'5','currency'=>'€'],
        ];
        foreach($rentals as $rental){
            \App\Property::create($rental);
        }



    }
}

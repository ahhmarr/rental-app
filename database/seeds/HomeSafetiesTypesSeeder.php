<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\HomesSafetiesType;
class HomeSafetiesTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $data=[
            'smoke detectors',
            'first aid kit',
            'fire extinguisher'
        ];
        foreach($data as $d){
            \App\Safety::create([
                'safety'  => $d,

            ]);
        }


    }
}

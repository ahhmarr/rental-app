<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\RentalListRentalPhoto;
class PropertyPhotosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $data=[
            ['rental_id'=>'1','rental_photo_url'=>'https://texasstation.sclv.com/~/media/Images/Page%20Background%20Images/Texas/TS_Hotel_King_lowrez.jpg'],
            ['rental_id'=>'1','rental_photo_url'=>'http://media-cdn.tripadvisor.com/media/photo-s/01/9a/68/50/hotel-rooms.jpg'],
            ['rental_id'=>'1','rental_photo_url'=>'http://cache.carlsonhotels.com/ow-cms/pkp/images/hotels/CHNBJCH/SuperiorRoom6502.jpg'],
            ['rental_id'=>'1','rental_photo_url'=>'http://www.kyreniahotels.co.uk/cratos/large/cratos-hotel-rooms-standard.jpg'],
            ['rental_id'=>'1','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSZ15qlJp5eKttejVnhX8OwzgbHvmmxj0nEyhxdjojlljZ6ZubG8Q'],
            ['rental_id'=>'1','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQRiPNLmVd-8Z_A27TdBdIgw8GzpOkNPRbglJLgjiRG8NQxLN_y'],

            ['rental_id'=>'2','rental_photo_url'=>'https://texasstation.sclv.com/~/media/Images/Page%20Background%20Images/Texas/TS_Hotel_King_lowrez.jpg'],
            ['rental_id'=>'2','rental_photo_url'=>'http://media-cdn.tripadvisor.com/media/photo-s/01/9a/68/50/hotel-rooms.jpg'],
            ['rental_id'=>'2','rental_photo_url'=>'http://cache.carlsonhotels.com/ow-cms/pkp/images/hotels/CHNBJCH/SuperiorRoom6502.jpg'],
            ['rental_id'=>'2','rental_photo_url'=>'http://www.kyreniahotels.co.uk/cratos/large/cratos-hotel-rooms-standard.jpg'],
            ['rental_id'=>'2','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSZ15qlJp5eKttejVnhX8OwzgbHvmmxj0nEyhxdjojlljZ6ZubG8Q'],
            ['rental_id'=>'2','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQRiPNLmVd-8Z_A27TdBdIgw8GzpOkNPRbglJLgjiRG8NQxLN_y'],

            ['rental_id'=>'3','rental_photo_url'=>'https://texasstation.sclv.com/~/media/Images/Page%20Background%20Images/Texas/TS_Hotel_King_lowrez.jpg'],
            ['rental_id'=>'3','rental_photo_url'=>'http://media-cdn.tripadvisor.com/media/photo-s/01/9a/68/50/hotel-rooms.jpg'],
            ['rental_id'=>'3','rental_photo_url'=>'http://cache.carlsonhotels.com/ow-cms/pkp/images/hotels/CHNBJCH/SuperiorRoom6502.jpg'],
            ['rental_id'=>'3','rental_photo_url'=>'http://www.kyreniahotels.co.uk/cratos/large/cratos-hotel-rooms-standard.jpg'],
            ['rental_id'=>'3','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSZ15qlJp5eKttejVnhX8OwzgbHvmmxj0nEyhxdjojlljZ6ZubG8Q'],
            ['rental_id'=>'3','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQRiPNLmVd-8Z_A27TdBdIgw8GzpOkNPRbglJLgjiRG8NQxLN_y'],

            ['rental_id'=>'4','rental_photo_url'=>'https://texasstation.sclv.com/~/media/Images/Page%20Background%20Images/Texas/TS_Hotel_King_lowrez.jpg'],
            ['rental_id'=>'4','rental_photo_url'=>'http://media-cdn.tripadvisor.com/media/photo-s/01/9a/68/50/hotel-rooms.jpg'],
            ['rental_id'=>'4','rental_photo_url'=>'http://cache.carlsonhotels.com/ow-cms/pkp/images/hotels/CHNBJCH/SuperiorRoom6502.jpg'],
            ['rental_id'=>'4','rental_photo_url'=>'http://www.kyreniahotels.co.uk/cratos/large/cratos-hotel-rooms-standard.jpg'],
            ['rental_id'=>'4','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSZ15qlJp5eKttejVnhX8OwzgbHvmmxj0nEyhxdjojlljZ6ZubG8Q'],
            ['rental_id'=>'4','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQRiPNLmVd-8Z_A27TdBdIgw8GzpOkNPRbglJLgjiRG8NQxLN_y'],

            ['rental_id'=>'5','rental_photo_url'=>'https://texasstation.sclv.com/~/media/Images/Page%20Background%20Images/Texas/TS_Hotel_King_lowrez.jpg'],
            ['rental_id'=>'5','rental_photo_url'=>'http://media-cdn.tripadvisor.com/media/photo-s/01/9a/68/50/hotel-rooms.jpg'],
            ['rental_id'=>'5','rental_photo_url'=>'http://cache.carlsonhotels.com/ow-cms/pkp/images/hotels/CHNBJCH/SuperiorRoom6502.jpg'],
            ['rental_id'=>'5','rental_photo_url'=>'http://www.kyreniahotels.co.uk/cratos/large/cratos-hotel-rooms-standard.jpg'],
            ['rental_id'=>'5','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSZ15qlJp5eKttejVnhX8OwzgbHvmmxj0nEyhxdjojlljZ6ZubG8Q'],
            ['rental_id'=>'5','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQRiPNLmVd-8Z_A27TdBdIgw8GzpOkNPRbglJLgjiRG8NQxLN_y'],

            ['rental_id'=>'6','rental_photo_url'=>'https://texasstation.sclv.com/~/media/Images/Page%20Background%20Images/Texas/TS_Hotel_King_lowrez.jpg'],
            ['rental_id'=>'6','rental_photo_url'=>'http://media-cdn.tripadvisor.com/media/photo-s/01/9a/68/50/hotel-rooms.jpg'],
            ['rental_id'=>'6','rental_photo_url'=>'http://cache.carlsonhotels.com/ow-cms/pkp/images/hotels/CHNBJCH/SuperiorRoom6502.jpg'],
            ['rental_id'=>'6','rental_photo_url'=>'http://www.kyreniahotels.co.uk/cratos/large/cratos-hotel-rooms-standard.jpg'],
            ['rental_id'=>'6','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSZ15qlJp5eKttejVnhX8OwzgbHvmmxj0nEyhxdjojlljZ6ZubG8Q'],
            ['rental_id'=>'6','rental_photo_url'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQRiPNLmVd-8Z_A27TdBdIgw8GzpOkNPRbglJLgjiRG8NQxLN_y'],
        ];

       foreach($data as $d){
        \App\PropertyPhoto::create($d);
    }


    }
}

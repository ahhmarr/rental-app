<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'guest','email'=>'guest@gmail.com','password'=>bcrypt('password')],
            ['name'=>'user','email'=>'user@gmail.com','password'=>bcrypt('password')],
            ['name'=>'admin','email'=>'admin@gmail.com','password'=>bcrypt('password')],
        ];
        foreach ($data as $d) {
            \App\User::create($d);
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\RoomsType;
class RoomTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $data=[
            'Entire Room/Apt',
            'Private Room',
            'Shared Room'
        ];
        foreach($data as $d){
            RoomsType::create([
                'room_type'  => $d,

            ]);
        }


    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('home_type');
            $table->string('room_type');
            $table->integer('bedroom');
            $table->integer('bed');
            $table->integer('bathroom');
            $table->integer('accommodate');
            $table->string('rate');
            $table->string('rate_extra_person');
            $table->string('rate_cleaning');
            $table->string('weekly_discount')->nullable();
            $table->string('monthly_discount')->nullable();
            $table->string('currency');
            $table->string('published')->default('0');
            $table->timestamps();
            $table->index('home_type','room_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}

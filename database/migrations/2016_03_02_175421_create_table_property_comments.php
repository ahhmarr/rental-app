<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePropertyComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('rental_id')->unsigned();
            $table->string('title');
            $table->text('text');
            $table->boolean('is_read')->default(0);
            $table->integer('rating_average')->default(0);
            $table->integer('rating_sleep')->default(0);
            $table->integer('rating_location')->default(0);
            $table->integer('rating_service')->default(0);
            $table->integer('rating_clearness')->default(0);
            $table->integer('rating_room')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_comments');
    }
}

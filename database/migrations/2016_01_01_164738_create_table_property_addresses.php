<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePropertyAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_addresses', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->integer('rental_id')->unsigned();
            $table->string('search_address')->nullable();
            $table->string('street_number')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('state')->nullable();
            $table->string('country');
            $table->string('zip');
            $table->string('phone_number');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->timestamps();

        });
        // adding full text search index
        DB::statement('ALTER TABLE property_addresses ADD FULLTEXT full(search_address, street_number, address, city, state, country, zip)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_addresses');
    }
}

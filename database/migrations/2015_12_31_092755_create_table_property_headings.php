<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePropertyHeadings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_headings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rental_id')->unsigned();
            $table->string('heading');
            $table->text('summary')->nullable();
            $table->text('activity')->nullable();
            $table->timestamps();
            $table->index('rental_id');

            $table->foreign('rental_id')
                    ->references('id')
                    ->on('properties')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_headings');
    }
}

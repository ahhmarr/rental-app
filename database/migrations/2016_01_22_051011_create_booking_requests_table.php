<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rental_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('message_thread_id')->unsigned()->nullable();
            $table->date('start');
            $table->date('end');
            $table->integer('room')->unsigned();
            $table->integer('guest')->unsigned();
            //$table->text('message')->nullable();
            $table->boolean('confirm')->default(false);
            $table->boolean('cancel')->defauil(false);
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_requests');
    }
}

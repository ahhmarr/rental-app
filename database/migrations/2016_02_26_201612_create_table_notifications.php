<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotifications extends Migration
{
    /**
     * Run the migrations.
     * Table schema
     * the `object_id` and `object_type` fields will be used to give a Notification some context.
     * For example, if you’re notifying a User that their Food Recipe with an ID of 168 has been favorited,
     * the `object_id` would be 168 and the `object_type` would be Recipe.
     *
     * the type field can be anything you like. It gives you something to work with when looping through and
     * displaying notifications should you wish to display different types of notifications in different
     * manners.
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

            $table->string('type', 128)->nullable();
            $table->string('subject', 128)->nullable();
            $table->text('body')->nullable();

            $table->integer('object_id')->unsigned();
            $table->string('object_type', 128);

            $table->boolean('is_read')->default(0);
            $table->dateTime('sent_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}

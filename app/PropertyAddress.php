<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyAddress extends Model
{
    protected $fillable=[
        'rental_id',
        'search_address',
        'street_number',
        'address',
        'city',
        'state',
        'country',
        'zip',
        'phone_number'
    ];
    public function property(){
        return $this->belongsTo('App\Property');
    }
}

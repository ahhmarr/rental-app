<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 */
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','type'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates=['created_at'];

    public function userDetail()
    {
        return $this->hasOne('App\Profile','user_id');
    }

    // a user can have many notifications

    public function notifications()
    {
        return $this->hasMany('App\Notification', 'user_id');
    }

    public function newNotification()
    {
        $notification = new Notification;
        $notification->user()->associate($this);

        return $notification;
    }

    // belongs to comments on property
    public function comments(){
        return $this->hasMany('App\PropertyComment', 'user_id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmenitiesType extends Model
{
    protected $fillable=[
        'amenity_type',
        'created_at',
        'updated_at'
    ];
}

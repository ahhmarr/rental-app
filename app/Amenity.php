<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    protected $fillable=[
        'amenity_type_id',
        'amenity',
        'created_at',
        'updated_at',
    ];
}

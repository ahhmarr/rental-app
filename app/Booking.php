<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable=[
        'rental_id',
        'user_id',
        'start',
        'end',
        'room',
        'guest',
        'message',
    ];
    public $dates=['start','end'];
    public function booking(){
        return $this->belongsTo('App\Property','rental_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}

<?php
/*
 * By default, the routes.php file contains a single route as well as a route group that applies
 *  the web middleware group to all routes it contains. This middleware group provides
 *  session state and CSRF protection to routes.
 * */
Route::group(['middleware' => 'web'], function () {
    Route::auth();

    // Notification Controller and notifications
    Route::get('/notification', 'NotificationController@show');
    Route::post('/notification/store', 'NotificationController@store');

    // property comment or reviews
    Route::post('/property/{id}/comment', 'CommentController@store');


    // Pages Controller, routes publicly open pages
    Route::get('/', 'PagesController@homePage');
    Route::get('/home', 'PagesController@homePage');
    Route::get('cancellation-policy', 'PagesController@cancelPolicy');

    // Search Properties
    Route::post('search','SearchPropertiesController@search');
    Route::get('search-all', 'SearchPropertiesController@listAllRentals');


    //House Owner Listing for rentals
    Route::get('property-list','PropertyController@propertyList');
    //Property Details
    Route::get('property/new', 'PropertyController@newPropertyForm');
    Route::post('property/new', 'PropertyController@store');
    Route::get('property/new/photos', 'PropertyController@rentalPhotos');
    Route::post('property/new/photos', 'PropertyController@saveRentalPhotos');
    Route::post('property/new/map', 'PropertyController@saveMap');
    Route::get('property/{id}','PropertyController@view');

    // property edit
    Route::get('property/{id}/edit','PropertyController@edit');
    Route::post('property/basic-details/{id}/edit','PropertyController@updatePropertyBasicDetails');
    Route::post('property/heading/{id}/edit','PropertyController@updatePropertyHeading');
    Route::post('property/address/{id}/edit','PropertyController@updatePropertyAddress');
    Route::post('property/map/{id}/edit','PropertyController@updatePropertyMapLocation');
    Route::post('property/amenities/{id}/edit','PropertyController@updatePropertyAmenities');
    Route::post('property/pricing/{id}/edit','PropertyController@updatePropertyPricing');
    Route::post('property/booking/{id}/edit','PropertyController@updatePropertyBooking');
    Route::post('property/photos/{id}/edit','PropertyController@updatePropertyPhoto');

    // New Property Wizard
    Route::get('new-property', 'NewPropertyWizard@index');
    Route::post('new-property', 'Auth\NewPropertyWizardAuth@register');;
    Route::get('new-property/stepTwo', 'NewPropertyWizard@registerUser');
    Route::post('new-property/house-properties', 'NewPropertyWizard@houseProperties');
    Route::post('new-property/address', 'NewPropertyWizard@address');
    Route::post('new-property/photos', 'NewPropertyWizard@savePhotos');
    Route::post('new-property/map', 'NewPropertyWizard@saveMap');



    Route::group(['prefix'=>'admin'],function()
    {
        // Admin Dashboard
        Route::get('', 'PagesController@adminPanel');
        //Admin Dashboard - Adding house properties
        Route::post('/add-home-type', 'PropertyAttributesController@addHomeType');
        Route::post('/add-room-type', 'PropertyAttributesController@addRoomType');
        Route::post('/add-room-and-bed-type', 'PropertyAttributesController@addRoomAndBedType');
        Route::post('/add-amenities-type', 'PropertyAttributesController@addAmenitiesType');
        Route::post('/add-amenity', 'PropertyAttributesController@addAmenity');
        Route::post('/add-home-safety-type', 'PropertyAttributesController@addHomeSafety');

        //Admin Dashboard - Deleting Property Attribute
        Route::delete('delete-home-type', 'PropertyAttributesController@deleteHomeType');

        //Admin Dashboard -  List Property
        Route::get('/home-types', 'PropertyAttributesController@homeTypes');
        Route::get('/room-types', 'PropertyAttributesController@roomTypes');
        Route::get('/rooms-and-beds-types', 'PropertyAttributesController@roomsAndBedsTypes');
        Route::get('/amenities-types', 'PropertyAttributesController@amenitiesTypes');
        Route::get('/amenities', 'PropertyAttributesController@amenities');
        Route::get('/house-safeties', 'PropertyAttributesController@houseSafeties');

        // Admin Dashboard list property
        Route::get('property-list', 'PropertyController@propertyList');
        Route::get('property/{id}','PropertyController@view');
    });

    /* Booked and Booking Requests */
    Route::post('booking-request','BookingsController@bookingRequest');
    Route::get('booking-request','BookingsController@requestList');
    Route::get('booking-confirm/{id?}','BookingsController@bookingConfirm');
    Route::get('booking','BookingsController@booking');

    /*Tour Booking and Booking Requests*/
    Route::get('tour-request', 'TourBookingsController@requestList');
    Route::get('tour-booking', 'TourBookingsController@booking');
    Route::get('tour-request-delete/{id}', 'TourBookingsController@deleteRequest');

    /* Profile Management */
    Route::get('profile/edit', 'ProfilesController@edit');
    Route::patch('profile/edit', 'ProfilesController@update');
    Route::post('profile/edit-pic', 'ProfilesController@editProfilePic');
    Route::get('profile/{id}', 'ProfilesController@view');

    /* Messages Routes */
    Route::get('message','MessagesController@viewThreads');
    Route::get('message/{id}', 'MessagesController@chats');
    Route::post('message/{id}/new-chat', 'MessagesController@newChat');

    /*generated automatically by make:auth command. no use yet.
     but do not remove*/
	Route::get('/home', 'HomeController@index');
});

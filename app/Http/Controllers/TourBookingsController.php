<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Property;
use App\BookingRequest;
use App\Booking;
use App\Http\Controllers\Controller;

class TourBookingsController extends InitController
{
    public function requestList(){
        $user_id = $this->user->id;
        $bookingRequests = BookingRequest::where('user_id','=',$user_id)->where('confirm','=','0')
            ->get();
        //dd($bookingRequests);
        return view('tour.apply', compact('bookingRequests'));
    }

    public function booking(){
        $user_id = $this->user->id;
        $bookings = Booking::where('user_id','=',$user_id)->get();
        return view('tour.approve', compact('bookings'));
    }

    public function deleteRequest($id){
        $booking = BookingRequest::findOrFail($id);
        $booking->delete();
        return redirect(url('tour-request'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Profile;
use App\PropertyAddress;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Amenity;
use App\HomeType;
use App\Safety;
use App\RoomsType;
use App\RoomsAndBed;
use App\Property;
use App\PropertyPhoto;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NewPropertyWizard extends Controller
{
    public function index(){
        $stepOne = true;
        return view('properties.wizard',compact('stepOne'));
    }

    // saving the step 1 of new-property-wizard i.e. new user
    public function registerUser(){
        $stepTwo = true;
        $user_id = Auth::user()->id;
        // creating blank profile of newly registered user
        Profile::create([
            'user_id' => $user_id,
            'profile_pic' => 'img/default-user.jpg',
        ]);
        // fetching data for house-properties FORM
        $common_amenities = Amenity::where('amenity_type_id','1')->get();
        $additional_amenities = Amenity::where('amenity_type_id','2')->get();
        $special_amenities = Amenity::where('amenity_type_id','3')->get();
        $house_rules = Amenity::where('amenity_type_id','4')->get();
        $home_types = HomeType::all();
        $home_safeties_types = Safety::all();
        $room_types = RoomsType::all();
        $room_and_bed_types = RoomsAndBed::all();

        // sending user_id and data for form generation
        return view('properties.wizard',compact('user_id','stepTwo','common_amenities','additional_amenities','special_amenities','house_rules','home_types','home_safeties_types','room_types','room_and_bed_types'));

    }

    // saving data of new rental wizard step -2 : house properties
    public function houseProperties(Requests\NewPropertyRequest $request){
        $stepThree = true;
        $input = $request->all();
        $input['user_id']= Auth::user()->id;

        // saved data in properties table
        $property = Property::create($input);
        $rental_id = $property->id; //  and fetched new $rental_id

        // save data in property_headings table
        $property->heading()->create($input);

        // save data in property_booking table
        $property->booking()->create($input);

        // save data in property_address_table
        $property->address()->create($input);

        // save data in property_amenities table
        if(isset($input['amenities_id']))
        {
            foreach($input['amenities_id'] as $input['amenity_id'])
            {
                $property->amenities()->create($input);
            }
        }

        //save data in property_safeties table
        if(isset($input['safeties_id']))
        {
            foreach($input['safeties_id'] as $input['safety_id'])
            {
                $property->safeties()->create($input);
            }
        }
        // concatenate city+state+county=address to send to map in next wizard window
        $address = $input['city'].','.$input['state'].','.$input['country'];
        // send newly generated rental_id adn user_id
        return view('properties.wizard',compact('rental_id','stepThree','address'));
    }

    // saving images of new rental wizard step - 4 : upload photos
    public function savePhotos(Request $request){
        $input = $request->all();
        if($request->file('file')->isValid()) {
            $extension = $input['file']->guessExtension();
            $destination_path = 'rentalPhotos/';
            $file_name = sha1(time() . time()) . ".{$extension}";
            $request->file('file')->move($destination_path, $file_name);
            $form['rental_photo_url'] =$destination_path.$file_name;
            $form['rental_id'] = $input['rental_id'];
            PropertyPhoto::create($form);
        }
        return 'false';
    }
    // saving latitude and longitude to the address
    public function saveMap(Request $request){
        $input  = $request->all();
        //check if rental id belongs to current user?
        $user_id = Property::findOrFail($input['rental_id'])->user_id;
        if(! $user_id == Auth::user()->id || $user_id == NULL)
            return url('/');
        //updating the coordinates
        PropertyAddress::where('rental_id',$input['rental_id'])
            ->update(['latitude'=>$input['lat'],'longitude'=>$input['lng']]);
        return redirect(url('property-list'));

    }

}

<?php

namespace App\Http\Controllers;

use App\PropertyComment;
use App\User;
use App\AmenitiesType;
use App\Amenity;
use App\HomeType;
use App\Notification\NewPropertyNotification;
use App\PropertyAddress;
use App\PropertyBooking;
use App\PropertyHeading;
use App\Property;
use App\PropertyPhoto;
use App\PropertyAmenity;
use App\PropertySafety;
use App\RoomsAndBed;
use App\RoomsType;
use App\Safety;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PropertyController extends Controller
{
    public function __construct()
    {
         $this->middleware('auth');
    }
    public function propertyList(){
        $user_id = Auth::user()->id;
        //dd($user_id);
        $properties = Property::orderBy('created_at','DESC')->get();
        return view('properties.list', compact('properties','user_id'));
    }

    public function view($id){
        $property = Property::findOrFail($id);

        // calculating average rating of property of all users
        // get the sum  of all the average_rating from the property_comments table
        // belongs to the current property.
        // then divide it by total number of reviews.
        $average_rating = PropertyComment::where('rental_id','=',$id)
                            ->select('rating_average')
                            ->get();
        // if any comment exists
        if (count($average_rating) > 0) {
            $property_rating = null;
            foreach ($average_rating as $rating) {
                $property_rating += $rating->rating_average;
            }
            $property_rating = $property_rating / count($average_rating);
            $number_of_reviews = count($average_rating);
            return view('properties.view', compact('property','property_rating','number_of_reviews'));
        }
        return view('properties.view', compact('property'));
    }

    public function newPropertyForm(){
        $common_amenities = Amenity::where('amenity_type_id','1')->get();
        $additional_amenities = Amenity::where('amenity_type_id','2')->get();
        $special_amenities = Amenity::where('amenity_type_id','3')->get();
        $house_rules = Amenity::where('amenity_type_id','4')->get();
        $home_types = HomeType::all();
        $home_safeties_types = Safety::all();
        $room_types = RoomsType::all();
        $room_and_bed_types = RoomsAndBed::all();

        // sending user_id and data for form generation
        return view('properties.new',compact('user_id','stepTwo','common_amenities','additional_amenities',
            'special_amenities','house_rules','home_types','home_safeties_types','room_types','room_and_bed_types'));
    }

    public function rentalPhotos()
    {
        $rental_id = 1;
        return view('properties.photos-upload', compact('rental_id'));
    }

    public function store(Requests\NewPropertyRequest $request){
        $input = $request->all();
        $input['user_id']= Auth::user()->id;

        // saved data in properties table
        $property = Property::create($input);
        //  and fetched new $rental_id
        $rental_id = $property->id;

        // save data in property_headings table
        $property->heading()->create($input);

        // save data in property_booking table
        $property->booking()->create($input);

        // save data in property_address_table
        $property->address()->create($input);

        // save data in property_amenities table
        if(isset($input['amenities_id']))
        {
            foreach($input['amenities_id'] as $input['amenity_id'])
            {
                $property->amenities()->create($input);
            }
        }

        //save data in property_safeties table
        if(isset($input['safeties_id']))
        {
            foreach($input['safeties_id'] as $input['safety_id'])
            {
                $property->safeties()->create($input);
            }
        }
        // concatenate city+state+county=address to send to map in next wizard window
        $address = $input['city'].','.$input['state'].','.$input['country'];

        // inserting a new notification
        $notification = new NewPropertyNotification();
        $notification->id = $rental_id;
        //dd($notification);
        $user = User::find(Auth::user()->id);
        $user->newNotification()
            ->withType('NewProperty')
            ->withSubject('New Property Added')
            ->withBody('<a style="color: #023D76" href="'.url('property/'.$rental_id).'">A new property</a>
                        has been added by you.')
            ->regarding($notification)
            ->deliver();

        return view('properties.photos-upload', compact('address', 'rental_id'));
    }

    public function saveRentalPhotos(Requests\RentalPhotosRequest $request){
        $input = $request->all();
        if($request->file('file')->isValid()) {
            $extension = $input['file']->guessExtension();
            $destination_path = 'rentalPhotos/';
            $file_name = sha1(time() . time()) . ".{$extension}";
            $request->file('file')->move($destination_path, $file_name);
            $form['rental_photo_url'] =$destination_path.$file_name;
            $form['rental_id'] = $input['rental_id'];
            PropertyPhoto::create($form);
        }
        return 'false';
    }

    // saving latitude and longitude to the address
    public function saveMap(Request $request){
        $input  = $request->all();
        //check if rental id belongs to current user?
        $user_id = Property::findOrFail($input['rental_id'])->user_id;
        if(! $user_id == Auth::user()->id || $user_id == NULL)
            return url('/');
        //updating the coordinates
        PropertyAddress::where('rental_id',$input['rental_id'])
            ->update(['latitude'=>$input['lat'],'longitude'=>$input['lng']]);
        return redirect(url('property-list'));

    }

    public function edit($id){
        $property = Property::findOrFail($id);
        $common_amenities = Amenity::where('amenity_type_id','1')->get();
        $additional_amenities = Amenity::where('amenity_type_id','2')->get();
        $special_amenities = Amenity::where('amenity_type_id','3')->get();
        $house_rules = Amenity::where('amenity_type_id','4')->get();
        $home_types = HomeType::all();
        $home_safeties_types = Safety::all();
        $room_types = RoomsType::all();
        $room_and_bed_types = RoomsAndBed::all();

        // sending data for form generation
        return view('properties.edit',compact('property','common_amenities','additional_amenities',
            'special_amenities','house_rules','home_types','home_safeties_types','room_types','room_and_bed_types'));
    }

    public function updatePropertyBasicDetails($id , Requests\EditBasicDetailRequest $request){
        $input = $request->all();
        $property = Property::findOrFail($id);
        $property['home_type'] = $input['home_type'];
        $property['room_type'] = $input['room_type'];
        $property['bedroom'] = $input['bedroom'];
        $property['bed'] = $input['bed'];
        $property['bathroom'] = $input['bathroom'];
        $property['accommodate'] = $input['accommodate'];
        $property->update();
        return redirect(url('property/'.$id.'/edit'));
    }

    public function updatePropertyHeading($id, Requests\EditHeadingRequest $request){
        $input = $request->all();
        $heading = PropertyHeading::whereRentalId($id)->first();
        $heading['heading'] = $input['heading'];
        $heading['summary'] = $input['summary'];
        $heading['activity'] = $input['activity'];
        $heading->update();
        return redirect(url('property/'.$id.'/edit'));
    }

    public function updatePropertyAddress($id, Requests\EditAddressRequest $request){
        $input = $request->all();
        $address = PropertyAddress::whereRentalId($id)->first();
        $address['search_address'] = $input['search_address'];
        $address['street_number'] = $input['street_number'];
        $address['address'] = $input['address'];
        $address['city'] = $input['city'];
        $address['state'] = $input['state'];
        $address['country'] = $input['country'];
        $address['zip'] = $input['zip'];
        $address['phone_number'] = $input['phone_number'];
        $address->update();
        return redirect(url('property/'.$id.'/edit'));
    }

    public function updatePropertyMapLocation($id){
        return redirect(url('property/'.$id.'/edit'));
    }

    public function updatePropertyAmenities($id, Requests\EditAmenitiesRequest $request){
        $input = $request->all();
        $input['rental_id'] = $id;
        // first deleting all the previous amenities
        PropertyAmenity::whereRentalId($id)->delete();
        if(isset($input['amenities_id']))
        {
            foreach($input['amenities_id'] as $input['amenity_id'])
            {
                PropertyAmenity::create($input);
            }
        }
        // deleting all the previous safeties
        PropertySafety::whereRentalId($id)->delete();
        if(isset($input['safeties_id']))
        {
            foreach($input['safeties_id'] as $input['safety_id'])
            {
                PropertySafety::create($input);
            }
        }
        return redirect(url('property/'.$id.'/edit'));
    }

    public function updatePropertyPricing($id, Requests\EditPricingRequest $request){
        $input = $request->all();
        $property = Property::findOrFail($id);
        $property['rate'] = $input['rate'];
        $property['rate_extra_person'] = $input['rate_extra_person'];
        $property['rate_cleaning'] = $input['rate_cleaning'];
        $property['weekly_discount'] = $input['weekly_discount'];
        $property['monthly_discount'] = $input['monthly_discount'];
        $property['currency'] = $input['currency'];
        $property->update();
        return redirect(url('property/'.$id.'/edit'));
    }

    public function updatePropertyBooking($id, Requests\EditBookingRequest $request){
        $input = $request->all();
        $booking = PropertyBooking::whereRentalId($id)->first();
        $booking['check_in_time'] = $input['check_in_time'];
        $booking['check_out_time'] = $input['check_out_time'];
        $booking['security_deposit'] = $input['security_deposit'];
        $booking['cancel_policy'] = $input['cancel_policy'];
        $booking->update();
        return redirect(url('property/'.$id.'/edit'));
    }

    public function updatePropertyPhoto($id, Requests\EditPhotoRequest $request){
        $input = $request->all();
        array_forget($input , '_token');
        PropertyPhoto::whereIn('id',$input['photos'])->delete();
        return redirect(url('property/'.$id.'/edit'));
    }
}

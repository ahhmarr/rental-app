<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function homePage()
    {
        return view('welcome');
    }

    public function cancelPolicy()
    {
        return view('cancellation-policy');
    }

    public function adminPanel(){
        return view('admin.dashboard');
    }

}

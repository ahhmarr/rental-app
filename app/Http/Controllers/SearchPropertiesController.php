<?php

namespace App\Http\Controllers;

use App\PropertyAddress;
use Illuminate\Http\Request;
use App\Property;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchPropertiesController extends Controller
{
    public function search(Request $request){
        $input = $request->all();
        $address = $input['address'];
        $results = PropertyAddress::select('rental_id')
            ->whereRaw("MATCH(search_address, street_number, address, city, state, country, zip) AGAINST(? IN BOOLEAN MODE)", array($address))
            ->get();
        $rentals = $results->pluck('rental_id');
       // dd($rentals);
        $properties = Property::whereIn('id',$rentals)->get();
       // $properties->all();
        //dd($properties);
        return view('search.list',compact('properties','address'));
    }

    public function listAllRentals(){
        $properties = Property::orderBy('created_at','DESC')->get();
        $address = 'List All rentals';
        return view('search.list', compact('properties','address'));
    }
}

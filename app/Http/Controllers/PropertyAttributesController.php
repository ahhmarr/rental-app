<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\HomeType;
use App\RoomsType;
use App\RoomsAndBed;
use App\AmenitiesType;
use App\Amenity;
use App\Safety;

class PropertyAttributesController extends Controller
{
    public function addHomeType(){
        $input = Request::all();
        HomeType::create($input);
        return redirect('admin/home-types');
    }

    public function addRoomType(){
        $input = Request::all();
        RoomsType::create($input);
        return redirect('admin/room-types');
    }

    public function addRoomAndBedType(){
        $input = Request::all();
        RoomsAndBed::create($input);
        return redirect('admin/rooms-and-beds-types');
    }

    public function addAmenitiesType(){
        $input = Request::all();
        AmenitiesType::create($input);
        return redirect('admin/amenities-types');
    }

    public function addAmenity(){
        $input = Request::all();
        Amenity::create($input);
        return redirect('admin/amenities');
    }

    public function addHomeSafety(){
        $input = Request::all();
        Safety::create($input);
        return redirect('admin/house-safeties');
    }

    public function homeTypes(){
        $home_types = HomeType::all();
        return view('admin.list-attributes', compact('home_types'));
    }
    public function roomTypes(){
        $room_types = RoomsType::all();
        return view('admin.list-attributes', compact('room_types'));
    }
    /* public function roomsAndBedsTypes(){
         $rooms_and_beds_types = RoomsAndBed::all();
         return view('admin.list-attributes', compact('rooms_and_beds_types'));
     }*/
    public function amenitiesTypes(){
        $amenities = Amenity::all();
        $amenities_types = AmenitiesType::all();
        return view('admin.list-attributes', compact('amenities_types','amenities'));
    }
    public function amenities(){
        $amenities = Amenity::all();
        $amenities_types = AmenitiesType::all();
        return view('admin.list-attributes', compact('amenities','amenities_types'));
    }
    public function houseSafeties(){
        $house_safeties = Safety::all();
        return view('admin.list-attributes', compact('house_safeties'));
    }

    public function deleteHomeType($id){
        $home_type = HomeType::findOrFail($id);
        $home_type->delete();
        return redirect('admin/add-house-properties/home-types');
    }
}

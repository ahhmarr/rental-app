<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfilesController extends InitController
{
    public function view($id){
        $profile = Profile::where('user_id',$id)->first();
        return view('profile.view-profile',compact('profile'));
    }

    public function edit(){
        $user_id = $this->user->id;
        $profile = Profile::where('user_id',$user_id)->first();
        return view('profile.edit-profile', compact('profile'));
    }

    public function editProfilePic(Request $request){
        $input = $request->all();
        $user_id = $this->user->id;
        if($request->file('file')->isValid()) {
            $extension = $input['file']->guessExtension();
            $destination_path = 'profilePic/';
            $file_name = sha1(time() . time()) . ".{$extension}";
            $request->file('file')->move($destination_path, $file_name);
            Profile::where('user_id',$user_id)->update(['profile_pic'=>$destination_path.$file_name]);
           // $form['profile_pic'] =$destination_path.$file_name;
           // Profile::create($form);
        }
        return 'false';
    }

    public function update(Request $request){
        $input = $request->all();
        $user_input = array_pull($input, 'name');
        $user_id = $this->user->id;
        $profile = Profile::findOrFail($user_id);
        $profile->update($input);

        DB::table('users')
            ->where('id', $user_id)
            ->update(['name' => $user_input]);
        return redirect(url('profile/'.$user_id));
    }
}

<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class NotificationController extends InitController
{
    public function store(Requests\StoreNotificationRequest $request){
    }

    public function show(){
        $user = $this->user;;
        $unreadNotifications = $user->notifications()->unread()->orderBy('created_at','desc')->get();
        return view('notifications.show', compact('unreadNotifications'));
    }
}

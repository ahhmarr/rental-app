<?php

namespace App\Http\Controllers;

use App\User;
use App\Booking;
use App\BookingRequest;
use App\Message;
use App\MessageThread;
use App\Notification;
use App\Property;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BookingsController extends InitController
{
    public function bookingRequest(Request $request){
        // check in user is logged in
        if(!$this->user){
            return redirect(url('login'));
        }
        $input = $request->all();
        // converting date format for mysql
        $input['start'] = date("Y-m-d", strtotime($input['start']));
        $input['end'] = date("Y-m-d", strtotime($input['end']));
        $input['user_id']= $this->user->id;

        // checking if owner trying to book his own rental
        $rental_owner_id = Property::select('user_id')
            ->where('id','=',$input['rental_id'])->first();
        $user_id = $this->user->id;
        if($user_id == $rental_owner_id->user_id)
            return redirect(url('/'));

        $booking_request = $input;
        // removing 'message' data from $input array
        array_forget($booking_request,'message');
        //create booking request
        $booking_id = BookingRequest::create($booking_request);

        // getting the user_id of owner of rental_id of booking request
        $receiver_id = Property::where('id','=',$input['rental_id'])->first();

        // preparing array for message_thread table
        $thread['booking_id'] = $booking_id->id;
        $start = Carbon::parse($input['start']);
        $end = Carbon::parse($input['end']);
        $thread['title'] = 'Booking Request order #'.$booking_id->id.'<br>Date : '.$start->toFormattedDateString().' to '.$end->toFormattedDateString().
                            '<br>Rooms : '.$input['room'].' Guest : '.$input['guest'].'<br>Address : '.$receiver_id->address->city.
                            ', '.$receiver_id->address->state.', '.$receiver_id->address->country;
        $thread['sender_id'] = $input['user_id'];
        $thread['receiver_id'] = $receiver_id->user_id;

        // inserting in message_threads table
        $message_thread = MessageThread::create($thread);

        // preparing array for messages table
        $message['thread_id'] = $message_thread->id;
        $message['sender_id'] = $input['user_id'];
        $message['message'] = $input['message'];

        // inserting into messages table
        Message::create($message);

        // inserting message_thread_id into booking_requests table
        $booking_id['message_thread_id'] = $message_thread->id;
        $booking_id->save();

        // inserting a new notification
        $notification = new Notification\BookingRequestNotification();
        $notification->id = $booking_id->id;
        //dd($notification);
        $user = User::find($rental_owner_id->user_id);
        $user->newNotification()
            ->withType('BookingRequest')
            ->withSubject('New Booking Request')
            ->withBody('<a style="color: #023D76" href="'.url('/profile/'.$this->user->id).'">'.$this->user->name.'</a>
                        <a href="'.url('/booking-request').'">has sent you a new booking request.</a>')
            ->regarding($notification)
            ->deliver();

        return redirect(url('tour-request'));
    }

    public function requestList(){
        $user_id = $this->user->id;
        $properties=Property::whereUserId($user_id)
                        ->get()->pluck("id")->toArray();
        $bookingRequests = BookingRequest::whereIn('rental_id',$properties)->where('confirm','=','0')
                                        ->get();
        return view('bookings.request', compact('bookingRequests'));
    }

    public function bookingConfirm($id){
        //getting the request details from booking_request table
        $booking_request = BookingRequest::findOrFail($id)->select('rental_id','user_id','start','end','room',
                            'guest','message_thread_id')->first()->toArray();
        //inserting request details in booking table i.e. confirming booking
        $bookingId = Booking::create($booking_request);
        // updating the confirm column in booking request table
        $booking_request = BookingRequest::findOrFail($id);
        $booking_request->confirm = true;
        $booking_request->save();

        // inserting a new notification booking confirmed
        /*$alert = [
            'recipient_id' => $booking_request->user_id,
            'sender_id' => $this->user->id,
            'activity_type' => '<div class="alert alert-warning">
                                    <i class="fa fa-rss"></i>
                                    <a href="'.url('/tour-booking').'">Your booking request  confirmed by&nbsp;
                                    </a>
                                    <a href="'.url('/profile/'.$this->user->id).'" style="color: #023D76">'.$this->user->name.'</a>
                                </div>',
            'object_type' => 'booking_confirmed',
            'object_url' => url('tour-booking')
        ];
        Notification::create($alert);*/
        $notification = new Notification\BookingConfirmNotification();
        $notification->id = $bookingId->id;
        //dd($notification);
        $user = User::find($booking_request->user_id);
        $user->newNotification()
            ->withType('BookingConfirm')
            ->withSubject('Booking Confirmed')
            ->withBody('<a style="color: #023D76" href="'.url('/profile/'.$this->user->id).'">'.$this->user->name.'</a>
                        <a href="'.url('/booking-request').'"> confirmed your booking request.</a>')
            ->regarding($notification)
            ->deliver();

        return redirect(url('booking'));
    }

    public function booking(){
        $user_id = $this->user->id;
        $properties = Property::whereUserId($user_id)
                        ->get()->pluck("id")->toArray();
        $bookings = Booking::whereIn('rental_id',$properties)->get();
        return view('bookings.confirm', compact('bookings'));
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
class InitController extends Controller
{
    public function __construct()
    {
        if(Auth::user()){
            $this->user=Auth::user();

        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Message;
use App\MessageThread;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MessagesController extends InitController
{
    public function viewThreads(){
        $user_id = $this->user->id;
        $message_threads = MessageThread::where('sender_id','=', $user_id)
            ->orWhere('receiver_id','=',$user_id)->get()->sortByDesc('updated_at');
        return view('messages.view',compact('message_threads'));
    }

    public function chats($id){
        // check if message id belongs to user id
        $user_id = $this->user->id;
        $message = Message::findOrFail($id)->thread()->first();
        if($message['sender_id']!= $user_id && $message['receiver_id']!= $user_id)
            return redirect(url('message'));

        // getting message of a particular thread
        $chats = Message::where('thread_id','=', $id)->get();
        $title = MessageThread::select('title','booking_id')
            ->where('id','=',$id)
            ->first();
        // check if current user is booking requester or rental owner
        // this is to decide whether to show "Confirm Booking Button" or not
        // if current_user == booking_request.user_id
        // i.e current user is booking requester ( Do not show "confirm booking button")
        if($user_id == $title->booking->user_id)
        {
            // booking_id  for route 'booking-confirm/{id?}'
            $title->booking_id = null;
        }
        // if booking is confirmed, then don't display button
        if($title->booking->confirm == true)
        {
            // booking_id for route 'booking-confirm/{id?}
            $title->booking_id = null;
        }
        //return $user_id;
        return view('messages.view',compact('chats','title'));

    }

    public function newChat($id, Request $request){
        // $this->validate($request, ['my_file'=>'required|mimes:txt,pdf,doc,docx,jpg,png|max:20000']);
        $message = $request->all();
        $upload = $request->file('my_file');
        $num = count($upload);
        //dd($id);

        // preparing array for insert in message table
        $message['thread_id'] = $id;
        $message['sender_id'] = $this->user->id;


        // if no files to attach, then insert message into table and
        // redirect to chat page
        if($num == 0)
        {
            $message['attachment'] = false;
            dd($message);
            $insert= Message::create($message);
            return redirect(url('message/'.$id));
        }
        else
        {
            $message['attachment'] = true;
            // to get insert id
            $insert = Message::create($message);
        }

        // upload files if files is present
        //dd($num);
        if($num > 0)
        {
            foreach ($request->file('my_file') as $file) {
                if($file != null)
                {
                    $extension = $file->guessExtension();
                    $destination_path = 'attachments/';
                    $file_name = sha1(time() . time()) . ".{$extension}";
                    $file->move($destination_path, $file_name);

                    // preparing array for attachments table
                    $attachment['message_id'] = $insert->id;
                    $attachment['file'] = $file->getClientOriginalName();
                    $attachment['link'] = $destination_path . $file_name;
                    Attachment::create($attachment);
                }
            }
        }
        return redirect(url('message/'.$id));

    }
}

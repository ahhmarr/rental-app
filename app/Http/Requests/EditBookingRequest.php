<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class EditBookingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }
        else
            return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'check_in_time'     =>  'required|string|max:20',
            'check_out_time'     =>  'required|string|max:20',
            'security_deposit'  =>  'required|string|max:4',
            'cancel_policy'     =>  'required|digits:1',
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class NewCommentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }
        else
            return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                 => 'required|string|max:50',
            'text'                  => 'required|string|max:100',
            'rating_sleep'          => 'numeric|max:5',
            'rating_location'       => 'numeric|max:5',
            'rating_service'        => 'numeric|max:5',
            'rating_clearness'      => 'numeric|max:5',
            'rating_room'           => 'numeric|max:5',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class EditAddressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }
        else
            return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search_address' => 'string|max:200',
            'street_number'  => 'string|max:200',
            'address'   =>  'required|string|max:200',
            'city'      =>  'required|string|max:200',
            'state'     =>  'string|max:200',
            'country'   =>  'required|string|max:200',
            'zip'       =>  'required|string|max:15',
            'phone_number'  =>  'string|max:15',
        ];
    }
}

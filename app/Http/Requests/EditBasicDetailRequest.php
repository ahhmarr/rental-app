<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class EditBasicDetailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::check())
        {
            return true;
        }
        else
            return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'home_type' => 'required|digits:1',
            'room_type' => 'required|digits:1',
            'bedroom' =>   'required|integer:max:50',
            'bed' =>       'required|integer:max:50',
            'bathroom' =>  'required|integer:max:50',
            'accommodate' => 'required|integer|max:50',
        ];
    }
}

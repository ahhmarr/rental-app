<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class NewPropertyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::check())
        {
            return true;
        }
        else
            return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'home_type' => 'required|digits:1',
            'room_type' => 'required|digits:1',
            'bedroom' =>   'required|integer:max:50',
            'bed' =>       'required|integer:max:50',
            'bathroom' =>  'required|integer:max:50',
            'accommodate' => 'required|integer|max:50',
            'heading' =>    'required|string|max:50',
            'summary' =>    'string|max:100',
            'activity' =>   'string|max:100',
            'search_address' => 'string|max:200',
            'street_number'  => 'string|max:200',
            'address'   =>  'required|string|max:200',
            'city'      =>  'required|string|max:200',
            'state'     =>  'string|max:200',
            'country'   =>  'required|string|max:200',
            'zip'       =>  'required|string|max:15',
            'phone_number'  =>  'string|max:15',
            'amenities_id.*'  =>  'digits_between:1,2',
            'safeties_id.*'   =>  'digits_between:1,2',
            'currency'      =>  'required|string|max:5',
            'rate'      =>  'required|integer',
            'rate_extra_person' =>  'required|integer',
            'rate_cleaning'     =>  'required|integer',
            'weekly_discount'   =>  'integer',
            'monthly_discount'  =>  'integer',
            'check_in_time'     =>  'required|string|max:20',
            'check_out_time'     =>  'required|string|max:20',
            'security_deposit'  =>  'required|string|max:4',
            'cancel_policy'     =>  'required|digits:1',
        ];
    }
}

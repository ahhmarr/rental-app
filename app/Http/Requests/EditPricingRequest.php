<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class EditPricingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }
        else
            return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rate'      =>  'required|integer',
            'rate_extra_person' =>  'required|integer',
            'rate_cleaning'     =>  'required|integer',
            'weekly_discount'   =>  'integer',
            'monthly_discount'  =>  'integer',
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreNotificationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (\Auth::check()) {
            return true;
        }
        else
            return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recipient_id' => 'numeric',
            'sender_id' => 'numeric',
            'activity_type' => 'string',
            'object_type' => 'string',
            'object_url' => 'string'
        ];
    }
}

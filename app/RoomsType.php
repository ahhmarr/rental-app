<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomsType extends Model
{
    protected $fillable=[
        'room_type',
        'created_at',
        'updated_at'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyHeading extends Model
{
    protected $fillable=[
        'rental_id',
        'heading',
        'summary',
        'activity',
        'created_at',
        'updated_at'
    ];
    public function property(){
        return $this->belongsTo('App\Property');
    }
}

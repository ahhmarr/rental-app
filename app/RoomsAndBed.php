<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomsAndBed extends Model
{
    protected $fillable=[
        'room_and_bed',
        'created_at',
        'updated_at'
    ];
}

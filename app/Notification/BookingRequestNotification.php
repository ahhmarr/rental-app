<?php namespace App\Notification;

class BookingRequestNotification
{
    public $object_id;
    public $id;
    public $object_type = 'BookingRequest';
}

?>
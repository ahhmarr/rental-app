<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingRequest extends Model
{
    use SoftDeletes;
    protected $fillable=[
      'rental_id',
        'user_id',
        'start',
        'end',
        'room',
        'guest',
        'message',
    ];
    //protected $dates = ['deleted_at'];
    protected $dates=['start','end','deleted_at'];
    public function bookingRequest(){
        return $this->belongsTo('App\Property','rental_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function messageThread(){
        return $this->hasOne('App\MessageThread','booking_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable=[
        'thread_id',
        'sender_id',
        'message',
        'attachment',
    ];

    protected $dates = ['deleted_at'];

    public function thread(){
        return $this->belongsTo('App\MessageThread','thread_id');
    }


    public function sender(){
        return $this->belongsTo('App\User','sender_id');
    }

    public function fileAttached(){
        return $this->hasMany('App\Attachment','message_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeType extends Model
{
    protected $fillable=[
        'home_type',
        'created_at',
        'updated_at'
    ];
}

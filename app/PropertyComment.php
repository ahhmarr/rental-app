<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyComment extends Model
{
    protected $fillable = [
        'user_id',
        'rental_id',
        'title',
        'text',
        'rating_sleep',
        'rating_location',
        'rating_service',
        'rating_clearness',
        'rating_room'
    ];

    protected $dates = [
        'created_at'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyBooking extends Model
{
    protected $fillable=[
        'check_in_time',
        'check_out_time',
        'security_deposit',
        'cancel_policy',
    ];
    public function property(){
        return $this->belongsTo('App\Property');
    }
}

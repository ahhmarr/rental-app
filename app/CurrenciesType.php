<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrenciesType extends Model
{
    protected $fillable=[
        'currency',
        'currency_code',
        'currency_symbol',
        'created_at',
        'update_at'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyAmenity extends Model
{
    protected $fillable=[
        'rental_id',
        'amenity_id',
        'created_at',
        'updated_at'
    ];

    /*amenity owned rental*/
    public function property(){
        return $this->belongsTo('App\Property');
    }
    public function amenity()
    {
        return $this->belongsTo('App\Amenity','amenity_id');
    }
}

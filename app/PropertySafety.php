<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertySafety extends Model
{
    protected $fillable=[
        'rental_id',
        'safety_id',
        'created_at',
        'updated_at'
    ];
    public function property(){
        return $this->belongsTo('App\Property');
    }
    public function safety(){
        return $this->belongsTo('App\Safety','safety_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable=[
        'user_id',
        'home_type',
        'room_type',
        'bedroom',
        'bed',
        'bathroom',
        'accommodate',
        'rate',
        'rate_extra_person',
        'rate_cleaning',
        'weekly_discount',
        'currency',
        'published'
    ];
    /*public function property(){
        return $this->belongsTo('App\Property');
    }*/

    public function homeType()
    {
        return $this->belongsTo('App\HomeType','home_type');
    }

    public function roomType()
    {
        return $this->belongsTo('App\RoomsType','room_type');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function address()
    {
        return $this->hasOne('App\PropertyAddress','rental_id');
    }
    public function amenities()
    {
        return $this->hasMany('App\PropertyAmenity','rental_id');
    }
    public function heading()
    {
        return $this->hasOne('App\PropertyHeading','rental_id');
    }
    public function photos()
    {
        return $this->hasMany('App\PropertyPhoto','rental_id');
    }
    public function safeties()
    {
        return $this->hasMany('App\PropertySafety','rental_id');
    }
    public function booking()
    {
        return $this->hasOne('App\PropertyBooking','rental_id');
    }

    public function comments(){
        return $this->hasMany('App\PropertyComment', 'rental_id');
    }
}

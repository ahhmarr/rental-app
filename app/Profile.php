<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id',
        'street',
        'address',
        'city',
        'state',
        'country',
        'zip',
        'phone_number',
        'about',
        'work',
        'profile_pic',
    ];

    public function users(){
        return $this->belongsTo('App\User','user_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyPhoto extends Model
{
    protected $fillable=[
        'rental_id',
        'rental_photo_url',
        'created_at',
        'updated_at'
    ];
    public function Property(){
        return $this->belongsTo('App\Property');
    }
}

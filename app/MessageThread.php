<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageThread extends Model
{
    protected $fillable=[
        'booking_id',
        'title',
        'sender_id',
        'receiver_id',
    ];

    protected $dates = ['deleted_at'];
    public function booking(){
        return $this->belongsTo('App\BookingRequest','booking_id');
    }

    public function messages(){
        return $this->hasMany('App\MessageThread','thread_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = [
      'message_id',
        'file',
        'link',
    ];
    public function message(){
        return $this->belongsTo('App\Message','message_id');
    }
}

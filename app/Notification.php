<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'user_id',
        'type',
        'subject',
        'body',
        'object_id',
        'object_type',
        'is_read',
        'sent_at'
    ];

    protected $dates = ['created_at', 'updated_at', 'sent_at'];

    public function getDates()
    {
        return [
            'sent_at',
            'create_at',
            'updated_at'
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function withSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    public function withBody($body)
    {
        $this->body = $body;

        return $this;
    }

    public function withType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function regarding($object)
    {
        if(is_object($object))
        {
            $this->object_id   = $object->id;
            $this->object_type = get_class($object);
        }

        return $this;
    }

    public function deliver()
    {
        $this->sent_at = new Carbon;
        $this->save();

        return $this;
    }

    /*
     * What we’re really interested in, is the count of the users unread notifications.
     *  To achieve this, we can add a scope to our Notification model.
     * */
    public function scopeUnread($query)
    {
        return $query->where('is_read', '=', 0);
    }
}

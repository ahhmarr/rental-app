@extends('layouts.admin-master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-11">
                <h4>List Of All Registered Rentals</h4>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>User ID</th>
                        <th>Country</th>
                        <th>Home Type</th>
                        <th>Room Type</th>
                        <th>Accommodate</th>
                        <th>Created At</th>
                        <th>Published</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $x=0; ?>
                    @foreach($rentals as $rental)
                        <tr>
                            <td>{{ ++$x }}</td>
                            <td>{{ $rental->user_id }}</td>
                            <td>{{ $rental->country }}</td>
                            <td>{{ $rental->home_type }}</td>
                            <td>{{ $rental->room_type }}</td>
                            <td>{{ $rental->accommodate }}</td>
                            <td>{{ $rental->created_at }}</td>
                            <td>@if($rental->published == '0')
                                    NO
                                    @else
                                    YES
                                @endif</td>
                            <td><a href="{{ url('admin/rental-view/'.$rental->id) }}" class="btn" role="link">view</a> | Delete | publish</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{ $rentals->links() }}
            </div>
        </div>
    </div>
    @stop
@extends('layouts.admin-master')
@section('content')
<div class="container">
    <div class="row">
        <h3>Rental Details</h3>
        <div class="col-md-9">
            <!-- user Detail -->
            <h4>User Detail</h4>
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr><td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td></tr>
                    @endforeach
                </tbody>
            </table>
            <hr/>
            <!-- Address details -->
            <h4>1. Address</h4>
            <table class="table">
                <thead>
                <tr>
                    <th>Search Address</th>
                    <th>Street Number</th>
                    <th>Address</th>
                    <th>city</th>
                    <th>State</th>
                    <th>country</th>
                    <th>zip</th>
                    <th>Phone</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($rentals as $rental)
                        <tr>
                            <td>{{ $rental->search_address }}</td>
                            <td>{{ $rental->street_number }}</td>
                            <td>{{ $rental->address }}</td>
                            <td>{{ $rental->city }}</td>
                            <td>{{ $rental->state }}</td>
                            <td>{{ $rental->country }}</td>
                            <td>{{ $rental->zip }}</td>
                            <td>{{ $rental->phone_number }}</td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
            <!--Heading & summary part -->
            <hr/>
            <h4>2. Heading & Summary</h4>
            @foreach($rentals as $rental)
                <p>Heading : {{ $rental->heading }}</p>
                <p>Summary : {{ $rental->summary }}</p>
                @endforeach
            <hr/>
            <h4>3. Rental Details</h4>
            <table  class="table">
                <thead>
                <tr>
                    <th>Home Type</th>
                    <th>Room Type</th>
                    <th>Bedrooms</th>
                    <th>Beds</th>
                    <th>Bathrooms</th>
                    <th>Accommodate</th>
                    <th>Currency</th>
                    <th>Rate</th>
                    <th>Published</th>
                </tr>
                </thead>
                <tbody>
                @foreach($rentals as $rental)
                    <tr>
                        <td>{{ $rental->home_type }}</td>
                        <td>{{ $rental->room_type }}</td>
                        <td>{{ $rental->bedroom }}</td>
                        <td>{{ $rental->bed }}</td>
                        <td>{{ $rental->bathroom }}</td>
                        <td>{{ $rental->accommodate }}</td>
                        <td>{{ $rental->currency }}</td>
                        <td>{{ $rental->rate }}</td>
                        <td>{{ $rental->published }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <hr/>
            <!-- Amenities Details -->
            <h3>4. Amenities</h3>
            @foreach($amenities as $amenity)
                <span>{{ $amenity->amenity }}</span>&nbsp;,&nbsp;
                @endforeach
            <hr/>
            <h3>5. Safeties Details</h3>
            @foreach($safeties as $safety)
                <span>{{ $safety->home_safety }}</span>&nbsp;,&nbsp;
                @endforeach
        </div>
    </div>
</div>
    @stop
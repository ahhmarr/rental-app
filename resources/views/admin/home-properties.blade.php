@extends('layouts.admin-master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>{!! Html::linkAction('PropertyAttributesController@homeTypes','Home Types') !!}</div>
                <div>{!! Html::linkAction('PropertyAttributesController@roomTypes','Room Types') !!}</div>
                <div>{!! Html::linkAction('PropertyAttributesController@amenities','Amenities') !!}</div>
                <div>{!! Html::linkAction('PropertyAttributesController@houseSafeties','House Safeties') !!}</div>
        </div>
    </div>
    </div>
@stop
@extends('layouts.admin-master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
            @if(isset($home_types))
                <h4>Home Types</h4>
                <button type="button" class="btn btn-primary btn-group-lg" data-toggle="modal" data-target="#home_types">
                    Add Home Types
                </button><br/><br/>
                <div class="modal fade" id="home_types" tabindex="-1" role="dialog" aria-labelledby="addHomeTypes">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="addHomeTypes">Add Home Types</h4>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['url'=>'admin/add-home-type']) !!}
                                {!! Form::hidden('form_name','home_types') !!}
                                <div class="form-group">
                                    {!! Form::label('home_type', 'Home Type') !!}
                                    {!! Form::text('home_type', '', ['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group">
                                    {!! Form::submit('Add Home Type', ['class'=>'btn btn-primary form-control']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                <?php $x=0 ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Home Type</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($home_types as $home_type)
                        <tr>
                            <td>{{ ++$x }}</td>
                            <td>{{ $home_type->home_type }}</td>
                            <td>{{ $home_type->created_at->diffForHumans() }}</td>
                            <td>{{ $home_type->updated_at->diffForHumans() }}</td>
                            <td>
                                edit | delete
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
            @if(isset($room_types))
                    <h4>Room Types</h4>
                    <button type="button" class="btn btn-primary btn-group-lg" data-toggle="modal" data-target="#room_types">
                        Add Room Types
                    </button><br/><br/>
                    <div class="modal fade" id="room_types" tabindex="-1" role="dialog" aria-labelledby="addRoomTypes">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="addRoomTypes">Add Room Types</h4>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open(['url'=>'admin/add-room-type']) !!}
                                    {!! Form::hidden('form_name','room_types') !!}
                                    <div class="form-group">
                                        {!! Form::label('room_type','Room Type') !!}
                                        {!! Form::text('room_type','',['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-group">
                                        {!! Form::submit('Add Room Type', ['class'=>'btn btn-primary form-control']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Room Type</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($room_types as $room_type)
                        <tr>
                            <td>{{ $room_type->id }}</td>
                            <td>{{ $room_type->room_type }}</td>
                            <td>{{ $room_type->created_at->diffForHumans() }}</td>
                            <td>{{ $room_type->updated_at->diffForHumans() }}</td>
                            <td>edit</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            @if(isset($rooms_and_beds_types))
            @endif
           {{-- @if(isset($amenities_types))

            @endif--}}
            @if(isset($amenities_types))
                <h4>Amenities</h4>
                    <button type="button" class="btn btn-primary btn-group-lg" data-toggle="modal" data-target="#add_amenity_type">
                        Add Amenity
                    </button><br/><br/>
                    <div class="modal fade" id="add_amenity_type" tabindex="-1" role="dialog" aria-labelledby="addAmenityType">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="addAmenityType">Add Amenity</h4>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open(['url'=>'admin/add-amenity']) !!}
                                    {!! Form::hidden('form_name','amenity') !!}
                                    <div class="form-group">
                                        {!! Form::label('amenity','Amenity') !!}
                                        {!! Form::text('amenity','',['class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        @foreach($amenities_types as $amenity_type)
                                            <input type="radio" name="amenity_type_id" value="{{ $amenity_type->id }}" >{{ $amenity_type->amenity_type }}
                                        @endforeach
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-group">
                                        {!! Form::submit('Add Amenity', ['class'=>'btn btn-primary form-control']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Amenity</th>
                        <th>Amenity type Id</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($amenities as $amenity)
                        <tr>
                            <td>{{ $amenity->id }}</td>
                            <td>{{ $amenity->amenity }}</td>
                            <td>{{ $amenity->amenity_type_id }}</td>
                            <td>{{ $amenity->created_at->diffForHumans() }}</td>
                            <td>{{ $amenity->updated_at->diffForHumans() }}</td>
                            <td>edit</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            @if(isset($house_safeties))
                <h4>House Safeties</h4>
                    <button type="button" class="btn btn-primary btn-group-lg" data-toggle="modal" data-target="#add_home_safety_type">
                        Add House Safety Type
                    </button><br/><br/>
                    <div class="modal fade" id="add_home_safety_type" tabindex="-1" role="dialog" aria-labelledby="addHouseSafety">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="addHouseSafety">Add House Safety Types</h4>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open(['url'=>'admin/add-home-safety-type']) !!}
                                    {!! Form::hidden('form_name','home_safeties_type') !!}
                                    <div class="form-group">
                                        {!! Form::label('safety','Home Safety Type') !!}
                                        {!! Form::text('safety','',['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-group">
                                        {!! Form::submit('Add Home Safety', ['class'=>'btn btn-primary form-control']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Home Safety</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($house_safeties as $safety)
                        <tr>
                            <td>{{ $safety->id }}</td>
                            <td>{{ $safety->safety }}</td>
                            <td>{{ $safety->created_at->diffForHumans() }}</td>
                            <td>{{ $safety->updated_at->diffForHumans() }}</td>
                            <td>edit</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            </div>
        </div>
    </div>
@stop
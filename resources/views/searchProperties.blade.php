<div class="tab-content">
    <div class="tab-pane fade in active" id="tab-1">
        <h2>Search and Save on Rentals</h2>
        <form action="{{ url('/search') }}" method="POST" enctype="multipart/form-data">
            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                <label>Where are you going?</label>
                <input class="typeahead form-control" name="address" id="address" placeholder="City or Zip Code" type="text" required />
            </div>
            <div class="input-daterange" data-date-format="M d, D">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                            <label>Check-in</label>
                            <input class="form-control" name="start" type="text" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                            <label>Check-out</label>
                            <input class="form-control" name="end" type="text" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-lg form-group-select-plus">
                            <label>Rooms</label>
                            <select class="form-control">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option selected="selected">4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                                <option>13</option>
                                <option>14</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-lg form-group-select-plus">
                            <label>Guests</label>
                            <select class="form-control">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option selected="selected">4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                                <option>13</option>
                                <option>14</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary btn-lg" type="submit">Search for Rentals</button>
        </form>
    </div>
</div>

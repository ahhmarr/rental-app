@extends('layouts.master')
@section('content')
    <div class="container">
        <h1 class="page-title">Cancellation Policy</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <aside class="sidebar-left">
                    <ul class="nav nav-pills nav-stacked nav-side mb30">

                        <li><a href="#flexible">Flexible</a></li>
                        <li class="active"><a href="#moderate">Moderate</a></li>
                        <li><a href="#strict">Strict</a></li>
                        <li><a href="#strict2">Super Strict 30 Days</a></li>
                        <li><a href="#strict3">Super Strict 60 Days</a></li>
                        <li><a href="#long_term">Long Term</a></li>
                    </ul>
                </aside>
            </div>
            <div class="col-md-9">
                <p>Kingdom of Rentals allows hosts to choose among three standardized cancellation policies (Flexible, Moderate, and Strict)
                    that we will enforce to protect both guest and host alike. The Super Strict cancellation policies apply to
                    special circumstances and are by invitation only. The Long Term cancellation policy applies to all
                    reservations of 28 nights or more. Each listing and reservation on our site will clearly state the
                    cancellation policy. Guests may cancel and review any penalties by viewing their travel plans and
                    then clicking 'Cancel' on the appropriate reservation.</p>
                <div class="gap gap-small"></div>
                <hr>
                <div class="row row-wrap">
                    <h4><a name="flexible"><strong>Flexible: </strong> refund 1 day prior to arrival, except fees</a></h4>
                    <ul>
                        <li>Cleaning fees are always refunded if the guest did not check in.</li>
                        <li>The Kingdom of Rentals service fee is non-refundable.</li>
                        <li>If there is a complaint from either party, notice must be given to Kingdom of Rentals within 24 hours of check-in.</li>
                        <li>Kingdom of Rentals will mediate when necessary, and has the final say in all disputes.</li>
                        <li>A reservation is officially canceled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard > Your Trips > Change or Cancel.</li>
                        <li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>
                        <li>Applicable taxes will be retained and remitted.</li>
                    </ul>
                </div>
                <hr>
                <div class="row row-wrap">
                    <h4><a name="moderate"><strong>Moderate:</strong> Full refund 5 days prior to arrival, except fees</a></h4>
                    <ul>
                        <li>Cleaning fees are always refunded if the guest did not check in.</li>
                        <li>The Kingdom of Rentals service fee is non-refundable.</li>
                        <li>If there is a complaint from either party, notice must be given to Kingdom of Rentals within 24 hours of check-in.</li>
                        <li>Kingdom of Rentals will mediate when necessary, and has the final say in all disputes.</li>
                        <li>A reservation is officially canceled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard > Your Trips > Change or Cancel.</li>
                        <li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>
                        <li>Applicable taxes will be retained and remitted.</li>
                    </ul>
                </div>
                <hr>
                <div class="row row-wrap">
                    <h4><a name="strict"><strong>Strict:</strong> 50% refund up until 1 week prior to arrival, except fees</a></h4>
                    <ul>
                        <li>Cleaning fees are always refunded if the guest did not check in.</li>
                        <li>The Kingdom of Rentals service fee is non-refundable.</li>
                        <li>If there is a complaint from either party, notice must be given to Kingdom of Rentals within 24 hours of check-in.</li>
                        <li>Kingdom of Rentals will mediate when necessary, and has the final say in all disputes.</li>
                        <li>A reservation is officially canceled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard > Your Trips > Change or Cancel.</li>
                        <li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>
                        <li>Applicable taxes will be retained and remitted.</li>
                    </ul>
                </div>
                <hr>
                <div class="row row-wrap">
                    <h4><a name="strict2"><strong>Super Strict 30 Days:</strong> 50% refund up until 30 days prior to arrival, except fees</a></h4>
                    <ul>
                        <li>Note: The Super Strict cancellation policy applies to special circumstances and is by invitation only.</li>
                        <li>Cleaning fees are always refunded if the guest did not check in.</li>
                        <li>The Kingdom of Rentals service fee is non-refundable.</li>
                        <li>If there is a complaint from either party, notice must be given to Kingdom of Rentals within 24 hours of check-in.</li>
                        <li>Kingdom of Rentals will mediate when necessary, and has the final say in all disputes.</li>
                        <li>A reservation is officially canceled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard > Your Trips > Change or Cancel.</li>
                        <li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>
                        <li>Applicable taxes will be retained and remitted.</li>
                    </ul>
                </div>
                <hr>
                <div class="row row-wrap">
                    <h4><a name="strict3"><strong>Super Strict 60 Days:</strong> 50% refund up until 60 days prior to arrival, except fees</a></h4>
                    <ul>
                        <li>Note: The Super Strict cancellation policy applies to special circumstances and is by invitation only.</li>
                        <li>Cleaning fees are always refunded if the guest did not check in.</li>
                        <li>The Kingdom of Rentals service fee is non-refundable.</li>
                        <li>If there is a complaint from either party, notice must be given to Kingdom of Rentals within 24 hours of check-in.</li>
                        <li>Kingdom of Rentals will mediate when necessary, and has the final say in all disputes.</li>
                        <li>A reservation is officially canceled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard > Your Trips > Change or Cancel.</li>
                        <li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>
                        <li>Applicable taxes will be retained and remitted.</li>
                    </ul>
                </div>
                <hr>
                <div class="row row-wrap">
                    <h4><a name="long_term"><strong>Long Term:</strong> First month down payment, 30 day notice for lease termination</a></h4>
                    <ul>
                        <li>Note: The Long Term cancellation policy applies to all reservations of 28 nights or more.</li>
                        <li>Cleaning fees are always refunded if the guest did not check in.</li>
                        <li>The Kingdom of Rentals service fee is non-refundable.</li>
                        <li>If there is a complaint from either party, notice must be given to Kingdom of Rentals within 24 hours of check-in.</li>
                        <li>Kingdom of Rentals will mediate when necessary, and has the final say in all disputes.</li>
                        <li>A reservation is officially canceled when the guest clicks the cancellation button on the cancellation confirmation page, which they can find in Dashboard > Your Trips > Change or Cancel.</li>
                        <li>Cancellation policies may be superseded by the Guest Refund Policy, safety cancellations, or extenuating circumstances. Please review these exceptions.</li>
                        <li>Applicable taxes will be retained and remitted.</li>
                    </ul>
                </div>
                <hr>
            </div>
        </div>
    </div>
    @stop
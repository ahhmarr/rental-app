@extends('layouts.properties-master')
@section('content')
    <div class="container">
        <h1 class="page-title">Notifications</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <aside class="sidebar-left">
                    <ul class="nav nav-pills nav-stacked nav-side mb30">
                        <li>
                        </li>
                    </ul>
                </aside>
                <div class="sidebar-left">
                </div>
            </div>
            <div class="col-md-9">
                {{-- getting all the unread notifications --}}
                @foreach($unreadNotifications as $notification)
                    @if($notification->type == 'BookingConfirm' )
                        <div class="alert alert-info">
                            <i class="fa fa-plane"></i>
                    @elseif($notification->type == 'BookingRequest')
                        <div class="alert alert-warning">
                            <i class="fa fa-rss"></i>
                    @elseif($notification->type == 'NewProperty')
                        <div class="alert alert-success">
                            <i class="fa fa-home"></i>
                    @endif
                        <strong>{!! $notification->subject !!}</strong>
                        <p class="subject"></p>
                        <p class="body">
                            {!! $notification->body !!}
                            {{ $notification->sent_at->diffForHumans() }}
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @stop
@extends('layouts.properties-master')
@section('content')
    <div class="container">
        <h1 class="page-title">Rental Bookings</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php $x=0; ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Name</th>
                        <th>Rooms</th>
                        <th>Guests</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Message</th>
                        <th>Cancel</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bookings as $booking)
                        <tr>
                            <th>{{ ++$x }}</th>
                            <th>{{ $booking->booking->heading->heading }}</th>
                            <th>{{ $booking->user->name }}</th>
                            <th>{{ $booking->room }}</th>
                            <th>{{ $booking->guest }}</th>
                            <th>{{ $booking->start->format('j-F \' y') }}</th>
                            <th>{{ $booking->end->format('j-F \'y') }}</th>
                            <th><a href="{{ url('message') }}" class="btn btn-primary btn-sm">view</a></th>
                            <th></th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                    @if($bookings->isEmpty())
                            <span>No Data Found</span>
                    @endif
                <!-- booking request message modals -->
                @foreach($bookings as $message)
                    <div class="modal fade" id="message{{ $message->id }}" tabindex="-1" role="dialog" aria-labelledby="message">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5>Message<button class="close" data-dismiss="modal">X</button></h5>
                                </div>
                                <div class="modal-body">
                                    {!! $message->message !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
@extends('layouts.properties-master')
@section('content')
    <div class="container">
        <h1 class="page-title">Rental Booking Requests</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                    <?php $x=0; ?>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Name</th>
                            <th>Rooms</th>
                            <th>Guests</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Message</th>
                            <th>Confirm Booking</th>
                            <th>Cancel</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bookingRequests as $request)
                            <tr>
                                <th>{{ ++$x }}</th>
                                <th>{{ $request->bookingRequest->heading->heading }}</th>
                                <th>{{ $request->user->name }}</th>
                                <th>{{ $request->room }}</th>
                                <th>{{ $request->guest }}</th>
                                <th>{{ $request->start->format('j-F \' y') }}</th>
                                <th>{{ $request->end->format('j-F \'y') }}</th>
                                <th><a href="{{ url('message') }}" class="btn btn-primary btn-sm">view</a></th>
                                <th><button id="confirm" href="booking-confirm/{{ $request->id }}" class="btn btn-success btn-sm">confirm</button></th>
                                <th></th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                        @if($bookingRequests->isEmpty())
                            <span><strong>No Data found</strong></span>
                        @endif
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.btn-success').click(function() {
                if (confirm('Confirm Booking, Are you sure?')) {
                    var url = $(this).attr('href');
                    location.href= url;
                }
            });
        });
    </script>
    @stop
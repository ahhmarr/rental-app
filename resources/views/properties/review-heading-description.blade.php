<div class="booking-item-meta">
@if(isset($property_rating))
        <h2 class="lh1em mt40">Exeptional!</h2>
        <h3>{{ ($property_rating/5)*100 }}% <small >of guests recommend</small></h3>
        <div class="booking-item-rating">
            <ul class="icon-list icon-group booking-item-rating-stars">
                {{--maximum possible rating is 5.
                    number of yellow faces  = $comment->rating
                    number of white faces = 5 - $comment->rating --}}
                @for($i = 1; $i<= $property_rating; $i++)
                    <li>
                        <i class="fa fa-star"></i>
                    </li>
                @endfor
            </ul><span class="booking-item-rating-number"><b >{{ $property_rating }}</b> of 5 <small class="text-smaller">guest rating</small></span>
            <p><a class="text-default" href="#">based on {{ $number_of_reviews }} reviews</a></p>
        </div>
@else

@endif
    <h3>Property description</h3>
    <p>{{ $property->heading->summary }}</p>
</div>

<div class="gap gap-small"></div>
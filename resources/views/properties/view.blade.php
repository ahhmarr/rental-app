@extends('layouts.properties-master')
@section('content')
    <div class="container">
        <div class="booking-item-details">
            <header class="booking-item-header">
                <div class="row">
                    <div class="col-md-9">
                        <h2 class="lh1em">
                            {{ $property->heading->heading }}
                        </h2>
                        <p class="lh1em text-small">
                            <i class="fa fa-map-marker"></i> 
                            {{ $property->address->search_address }}
                        </p>
                        <ul class="list list-inline text-small">
                            <li>
                                <a href="#">
                                    <i class="fa fa-envelope"></i> 
                                    Agent E-mail
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-home"></i> 
                                    Agent Website
                                </a>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i> 
                                {{ $property->address->phone_number }}
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <p class="booking-item-header-price">
                            <span class="text-lg">
                                {{ $property->currency }}{{ $property->rate }} 
                            </span> 
                            <span>/night</span>
                        </p>
                    </div>
                    <input type="hidden" id="latitude" value="{{ $property->address->latitude }}">
                    <input type="hidden" id="longitude" value="{{ $property->address->longitude }}">
                </div>
            </header>
            <div class="row">
                <div class="col-md-7">
                    <div class="tabbable booking-details-tabbable">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-camera"></i>Photos</a>
                            </li>
                           <li><a href="#google-map-tab" data-toggle="tab"><i class="fa fa-map-marker"></i>On the Map</a></li>
                        </ul>
                        <div class="tab-content">
                            @include('properties.photo-gallery')
                        

                            @include('properties.map-tab')
                        </div>
                    </div>
                </div>
                @include('properties.map')
                <div class="col-md-5">
                    @include('properties.review-heading-description')
                    @include('properties.booking-request')
                </div>
            </div>
            <div class="gap"></div>
            <div class="row">
                @include('properties.amenities',compact('property'))
                <div class="col-md-6">
                    <h3>Activity Around</h3>
                    <p>{{ $property->heading->activity }}</p>
                </div>
            </div>
            <hr/>
                @include('properties.price')
            <hr/>
                @include('properties.booking')
            <div class="gap gap-small"></div>
            @include('properties.review')
        </div>
        <div class="gap gap-small"></div>
    </div>
    @stop
<a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#book_request">Book Now</a>
<div class="modal fade" id="book_request" tabindex="-1" role="dialog" aria-labelledby="bookRequest">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Request Booking<button class="close" data-dismiss="modal">X</button>
                </h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url'=>'booking-request']) !!}
                <input type="hidden" name="rental_id" value="{{ $property->id }}">
                    <div class="input-daterange" data-date-format="M d, D">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                    <label>Check-in</label>
                                    <input class="form-control" name="start" type="text" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                    <label>Check-out</label>
                                    <input class="form-control" name="end" type="text" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-group-lg form-group-select-plus">
                                    <label for="rooms">Rooms</label>
                                    <select class="form-control" name="room" id="room" required>
                                        <option value="">Select</option>
                                        @for($i=1; $i<= $property->bedroom; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-group-lg form-group-select-plus">
                                    <label for="guests">Guests</label>
                                    <select class="form-control" name="guest" id="guest" required>
                                        <option value="">Select</option>
                                        @for($i=1;$i<= $property->accommodate; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                <label for="message">Message</label>
                <textarea name="message" id="message" cols="30" rows="10">
                </textarea>

            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-lg" type="submit">Request Booking</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#message').redactor();
    });
</script>
{!! Form::open(['url'=>'property/basic-details/'.$property->id.'/edit']) !!}
<div class="gap gap-mini"></div>
<div class="form-group form-group-lg form-group-select-plus col-md-3">
    <label for="home_type">Home Type:</label>
    <select class="form-control" name="home_type" id="home_type" required>
        <option value="{{ $property->homeType->id }}">{{ $property->homeType->home_type }}</option>
        <option value="">---------</option>
        @foreach($home_types as $home_type)
            <option value="{{ $home_type->id }}">{{ $home_type->home_type }}</option>
        @endforeach
    </select>
</div>
<div class="gap gap-small"></div>
<p>Room Type : <strong>{{ $property->roomType->room_type }}</strong></p>
@foreach($room_types as $room_type)
    <div class="radio-inline radio-lg">
        <label><input class="i-radio" type="radio" name="room_type"  value="{{ $room_type->id }}" required/>{{ $room_type->room_type }}</label>
    </div>
@endforeach
<div class="gap gap-small"></div>
<div class="form-group form-group-lg form-group-select-plus col-md-3">
    <label for="bedroom">Bedrooms:</label>
    <select class="form-control" name="bedroom" id="bedroom" required>
        <option value="{{ $property->bedroom }}">{{ $property->bedroom }}</option>
        <option value="">--------</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
    </select>
</div>
<div class="form-group form-group-lg form-group-select-plus col-md-3">
    <label for="bed">Beds: </label>
    <select class="form-control" name="bed" id="bed" required>
        <option value="{{ $property->bed }}">{{ $property->bed }}</option>
        <option value="">--------</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
    </select>
</div>
<div class="form-group form-group-lg form-group-select-plus col-md-3">
    <label for="bathroom">Bathrooms:</label>
    <select class="form-control" name="bathroom" id="bathroom" required>
        <option value="{{ $property->bathroom }}">{{ $property->bathroom }}</option>
        <option value="">--------</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="more">more</option>
    </select>
</div>
<div class="gap gap-mini"></div>
<div class="form-group form-group-lg form-group-select-plus col-md-3">
    <label for="accommodate">Accommodates:</label>
    <select class="form-control" name="accommodate" id="accommodate" required>
        <option value="{{ $property->accommodate }}">{{ $property->accommodate }}</option>
        <option value="">--------</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="more">more</option>
    </select>
</div>
<div class="gap gap-small"></div>
<input class="btn btn-primary" type="submit" value="Update" />
{!! Form::close() !!}
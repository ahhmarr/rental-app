<h3><strong>Bookings</strong></h3>
<div>
    check in time : {{ $property->booking->check_in_time }}
</div>
<div>
    check out time : {{ $property->booking->check_out_time }}
</div>
<div>
    Security Deposit : {{ $property->booking->security_deposit }}%
</div>
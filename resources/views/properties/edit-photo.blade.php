<p>Select photo that you want to delete</p>
{!! Form::open(['url' => 'property/photos/'.$property->id.'/edit']) !!}
@if($property->photos)
    @foreach($property->photos as $image)
<div class="checkbox col-md-3">
    <table>
        <tbody>
        <tr>
            <td style="">
                <input type="checkbox" value="{{ $image->id }}" name="photos[]" id="photos[]" class="i-check" style="position: absolute; opacity: 1; margin-top: 30px;">
            </td>
            <td>
                <img width="100px" height="100px" src="{{ asset($image->rental_photo_url) }}">
            </td>
        </tr>
        </tbody>
    </table>
</div>
    @endforeach
@endif
<div class="gap gap-small"></div>
<input class="btn btn-primary" type="submit" value="Delete" />
{!! Form::close() !!}
<div class="gap gap-small"></div>
<h3>Upload Photos of Your House/Rooms</h3>
{!! Html::script(asset('js/dropzone.js')) !!}
{!! Form::open(array('url'=>'/property/new/photos','class'=>'dropzone', 'id'=>'my-awesome-dropzone')) !!}
<input type="hidden" name="rental_id" value="{{ $property->id }}">
{!! Form::close() !!}
<div class="gap gap-small"></div>
<a href="{{ URL::to('property/'.$property->id.'/edit') }}" class="btn btn-primary">Update</a>
@extends('layouts.properties-master')
@section('content')
    <div class="container">
        <h1 class="page-title">New Rental</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <aside class="sidebar-left">
                    <ul class="nav nav-pills nav-stacked nav-side mb30">
                        <li><a href="#home-type">Home Type</a></li>
                        <li><a href="#room-type">Room Type</a></li>
                        <li><a href="#rooms-and-beds">Rooms And Beds</a></li>
                        <li><a href="#heading">Heading</a></li>
                        <li><a href="#activity">Activity</a></li>
                        <li><a href="#location">Address</a></li>
                        <li><a href="#amenities">Amenities</a></li>
                        <li><a href="#pricing">Pricing</a></li>
                        <li><a href="#bookings">Booking</a></li>
                    </ul>
                </aside>
            </div>
            <div class="col-md-9">
                @if($errors ->any())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif
                    {!! Form::open(['url'=>'property/new']) !!}
                    <p><a name="home-type"></a></p>
                    <h5><strong>Home Type</strong></h5>
                    <div class="gap gap-mini"></div>
                    <div class="form-group form-group-lg form-group-select-plus col-md-3">
                        <label for="home_type"></label>
                        <select class="form-control" name="home_type" id="home_type" required>
                            <option value="">Select</option>
                            @foreach($home_types as $home_type)
                                <option value="{{ $home_type->id }}">{{ $home_type->home_type }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="gap gap-small"></div>
                    <hr/>
                    <p><a name="room-type"></a></p>
                    <h5><strong>Room Type</strong></h5>
                    <div class="gap gap-mini"></div>
                    @foreach($room_types as $room_type)
                        <div class="radio-inline radio-lg">
                            <label><input class="i-radio" type="radio" name="room_type"  value="{{ $room_type->id }}" required/>{{ $room_type->room_type }}</label>
                        </div>
                    @endforeach
                    <hr/>
                    <p><a name="rooms-and-beds"></a></p>
                    <h5><strong>Rooms And Beds</strong></h5>
                    <div class="form-group form-group-lg form-group-select-plus col-md-3">
                        <label for="bedroom">Bedrooms:</label>
                        <select class="form-control" name="bedroom" id="bedroom" required>
                            <option value="">Select</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                        </select>
                    </div>
                    <div class="form-group form-group-lg form-group-select-plus col-md-3">
                        <label for="bed">Beds: </label>
                        <select class="form-control" name="bed" id="bed" required>
                            <option value="">Select</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                        </select>
                    </div>
                    <div class="form-group form-group-lg form-group-select-plus col-md-3">
                        <label for="bathroom">Bathrooms:</label>
                        <select class="form-control" name="bathroom" id="bathroom" required>
                            <option value="">Select</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="more">more</option>
                        </select>
                    </div>
                    <div class="gap gap-mini"></div>
                    <div class="form-group form-group-lg form-group-select-plus col-md-3">
                        <label for="accommodate">Accommodates:</label>
                        <select class="form-control" name="accommodate" id="accommodate" required>
                            <option value="">Select</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="more">more</option>
                        </select>
                    </div>
                    <div class="gap gap-small"></div>
                    <p><a name="heading"></a></p>
                    <h5><strong>Tell Guests about your Space</strong></h5>
                    <p>Every space has unique features. Highlight what makes your space welcoming and sparking for
                        guests who want to stay in your area.</p>
                    <div class="form-group form-group-lg">
                        <label><strong>Listing Heading</strong></label>
                        <input class="form-control" type="text" name="heading" id="listing_heading" maxlength="80" size="50" placeholder="Try adding unique features, activities or qualities of your Space eg= sunny beach front bedroom apartment"  required/>
                    </div>
                    <div class="form-group form-group-lg">
                        <label for="summary"><strong>Summary</strong></label>
                        <textarea name="summary" id="summary" cols="100" rows="6" maxlength="100" placeholder="Tell Guests what’s welcoming in your space. You can include details about attractions, amenities and Neighborhoods. " required></textarea>
                    </div>
                    <hr/>
                    <p><a name="activity"></a></p>
                    <h3><strong>Activity</strong></h3>
                    <p>Share about activities in your space and neighborhood to make your space more welcoming for The Guests.<br>Like <strong>tourist attractions</strong>, <strong>golf</strong>, <strong>museum</strong>
                        , <strong>beach</strong>, <strong>ski</strong> etc.</p>
                    <div class="form-group form-group-lg">
                        <label for="activity"><strong>activity</strong></label>
                        <textarea name="activity" id="activity" cols="100" rows="6" maxlength="100" placeholder="Tell your guest about activities or tourists attractions around. " required></textarea>
                    </div>
                    <hr/>
                    <p><a name="location"></a></p>
                    <h3><strong>Location</strong></h3>
                    <p>This information will help guest to find exact spot of your space</p>
                    <div id="locationField">
                        <label for="autocomplete">Search Address</label>
                        <input id="autocomplete" class="form-control" name="search_address" placeholder="Enter your address" onFocus="geolocate()" type="text" required />
                    </div>
                    <div class="form-group form-group-lg">
                        <label for="street_number">Street Number</label>
                        <input class="field form-control" name="street_number" id="street_number" disabled="true" required/>
                    </div>
                    <div class="form-group form-group-lg">
                        <label for="route">Address</label>
                        <input class="field form-control" name="address" id="route" disabled="true" required>
                    </div>
                    <div class="form-group form-group-lg">
                        <label for="locality">City</label>
                        <input class="field form-control" name="city" id="locality" disabled="true" required>
                    </div>
                    <div class="form-group form-group-lg">
                        <label for="administrative_area_level_1">State</label>
                        <input class="field form-control" name="state" id="administrative_area_level_1" disabled="true" required>
                    </div>
                    <div class="form-group form-group-lg">
                        <label for="country">Country</label>
                        <input class="field form-control" name="country" id="country" disabled="true" required>
                    </div>
                    <div class="form-group form-group-lg">
                        <label for="postal_code">ZIP</label>
                        <input class="field form-control" name="zip" id="postal_code" disabled="true" required>
                    </div>
                    <div class="form-group form-group-lg">
                        <label for="phone_number">Phone Number</label>
                        <input class="form-control" type="text" name="phone_number" id="phone_number"  placeholder="Phone Number" required/>
                    </div>
                    <hr/>
                    <p><a name="amenities"></a></p>
                    <h3><strong>Tell Guests about amenities, house safety and rules.</strong></h3>
                    <p>Every space has unique amenities and features. Highlight what makes your space welcoming and stand out for guests who want to stay in your area.</p>
                    <h5><strong>Common Amenities</strong></h5>
                    @foreach($common_amenities as $common_amenity)
                        <div class="checkbox">
                            <label><input class="i-check" type="checkbox" name="amenities_id[]" id="commonamenity{{ $common_amenity->id }}" value="{{ $common_amenity->id }}" />{{ $common_amenity->amenity }}</label>
                        </div>
                    @endforeach
                    <hr/>
                    <h5><strong>Additional Amenities</strong></h5>
                    @foreach($additional_amenities as $additional_amenity)
                        <div class="checkbox">
                            <label><input class="i-check" type="checkbox" name="amenities_id[]" id="amenity{{ $additional_amenity->id }}" value="{{ $additional_amenity->id }}" />{{ $additional_amenity->amenity }}</label>
                        </div>
                    @endforeach
                    <hr/>
                    <h5><strong>Special Features</strong></h5>
                    @foreach($special_amenities as $special_amenity)
                        <div class="checkbox">
                            <label><input class="i-check" type="checkbox" name="amenities_id[]" id="specialamenity{{ $special_amenity->id }}" value="{{ $special_amenity->id }}" />{{ $special_amenity->amenity }}</label>
                        </div>
                    @endforeach
                    <hr/>
                    <p><a name="safety-features"></a></p>
                    <h5><strong>House Safeties</strong></h5>
                    @foreach($home_safeties_types as $home_safety)
                        <div class="checkbox">
                            <label><input class="i-check" type="checkbox" name="safeties_id[]" id="homeSafety{{ $home_safety->id }}" value="{{ $home_safety->id }}" />{{ $home_safety->safety }}</label>
                        </div>
                    @endforeach
                    <hr/>
                    <h5><strong>House Rules</strong></h5>
                    @foreach($house_rules as $house_rule)
                        <div class="checkbox">
                            <label><input class="i-check" type="checkbox" name="amenities_id[]" id="homeSafety{{ $house_rule->id }}" value="{{ $house_rule->id }}" />{{ $house_rule->amenity }}</label>
                        </div>
                    @endforeach
                    <hr/>
                    <p><a name="pricing"></a></p>
                    <h3><strong>Pricing</strong></h3>
                    <p>You can set the price for your space, amenities and hospitality you will be providing.</p>
                    <div class="form-group form-group-lg form-group-select-plus col-md-3">
                        <label for="currency">Currency:</label>
                        <select class="form-control" name="currency" id="currency" required>
                            <option value="">Select</option>
                            <option value="$">USD</option>
                            <option value="₹">INR</option>
                            <option value="€">EURO</option>
                        </select>
                    </div>
                    <div class="gap gap-small"></div>
                    <div class="form-group form-group-lg">
                        <label><strong>Base Price Per Night</strong></label>
                        <input class="form-control" type="text" name="rate" id="rate"  placeholder="Price Rate" required />
                    </div>
                    <div class="form-group form-group-lg">
                        <label><strong>Rate per Extra Person</strong></label>
                        <input class="form-control" type="text" name="rate_extra_person" id="rate_extra_person"  placeholder="Price Rate"  required />
                    </div>
                    <div class="form-group form-group-lg">
                        <label><strong>Cleaning Charges</strong></label>
                        <input class="form-control" type="text" name="rate_cleaning" id="rate_cleaning"  placeholder="Price Rate" required />
                    </div>
                    <h5><strong>Long Stay Price:</strong></h5><p>You might want to offer discount for longer stay</p>
                    <div class="form-group form-group-lg">
                        <label><strong>Weekly Discount Percent (%)</strong></label>
                        <input class="form-control" type="text" name="weekly_discount" id="weekly_discount"  placeholder="Price Rate" required />
                    </div>
                    <div class="form-group form-group-lg">
                        <label><strong>Monthly Discount Percent (%)</strong></label>
                        <input class="form-control" type="text" name="monthly_discount" id="monthly_discount"  placeholder="Price Rate"  required />
                    </div>
                    <hr/>
                    <p><a name="bookings"></a></p>
                    <h3><strong>Bookings</strong></h3>
                    <div class="form-group form-group-lg">
                        <label><strong>Check In Time</strong></label>
                        <input class="form-control" type="text" name="check_in_time" id="check_in_time"  required />
                    </div>
                    <div class="form-group form-group-lg">
                        <label><strong>Check Out Time</strong></label>
                        <input class="form-control" type="text" name="check_out_time" id="check_out_time"  required />
                    </div>
                    <h3><strong>Security Deposit:</strong></h3>
                    <div class="form-group form-group-lg form-group-select-plus col-md-3">
                        <label for="security_deposit"></label>
                        <select class="form-control" name="security_deposit" id="security_deposit" required>
                            <option value="">Select</option>
                            <option value="10">10%</option>
                            <option value="15">15%</option>
                            <option value="20">20%</option>
                            <option value="25">25%</option>
                            <option value="30">30%</option>
                            <option value="40">40%</option>
                            <option value="50">50%</option>
                            <option value="100">100%</option>
                        </select>
                    </div>
                    <div class="gap gap-small"></div>

                    <h3><strong>Cancellation Policy</strong></h3>
                    <p>For more information on our cancellation policy, please <a href="{{ url('cancellation-policy') }}" target="_blank">click here</a></p>
                    <div class="form-group form-group-lg form-group-select-plus col-md-3">
                        <label for="cancel_policy"></label>
                        <select class="form-control" name="cancel_policy" id="cancel_policy" required>
                            <option value="">Select</option>
                            <option value="1">Flexible</option>
                            <option value="2">Moderate</option>
                            <option value="3">Strict</option>
                            <option value="4">Super Strict 30 Days</option>
                            <option value="5">Super Strict 60 Days</option>
                            <option value="6">Long Term</option>
                        </select>
                    </div>
                    <div class="gap gap-small"></div>
                    <hr/>
                    <input class="btn btn-primary" type="submit" value="Save & Proceed" />

                    {!! Form::close() !!}
                    {!! Html::script('js/map.js') !!}
                    <script type="text/javascript">
                        $('#check_in_time').timepicker({
                            template: false,
                            showInputs: false,
                            minuteStep: 5});
                        $('#check_out_time').timepicker({
                            template: false,
                            showInputs: false,
                            minuteStep: 5});</script>
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1nTKTEbC2EsR3fm9qnJ1_LD4QqmKG-WQ&signed_in=true&libraries=places&callback=initAutocomplete" async defer></script>
                </div>
            </div>
    </div>

@stop
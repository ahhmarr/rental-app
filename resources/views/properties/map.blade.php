<!-- google map -->
<script>
    var latitude = parseFloat($('#latitude').val());
    var longitude = parseFloat($('#longitude').val());
    console.log(latitude);
    console.log(longitude);
    function initMap() {
        var myLatLng = {lat: latitude, lng: longitude};
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
            zoom: 14,
            center: myLatLng
        });
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Hello World!'
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1nTKTEbC2EsR3fm9qnJ1_LD4QqmKG-WQ&signed_in=true&callback=initMap"></script>
{!! Form::open(['url' => 'property/heading/'.$property->id.'/edit']) !!}
<div class="form-group form-group-lg">
    <label for="listing_heading">Listing Heading:</label>
    <input class="form-control" type="text" name="heading" id="listing_heading" maxlength="80" size="50" value="{{ $property->heading->heading }}"  required/>
</div>
<div class="gap gap-small"></div>
<div class="form-group form-group-lg">
    <label for="summary">Summary:</label>
    <textarea name="summary" id="summary" cols="100" rows="6" maxlength="100" required>{{ $property->heading->summary }}</textarea>
</div>
<div class="gap gap-small"></div>
<div class="form-group form-group-lg">
    <label for="activity">Activity</label>
    <textarea name="activity" id="activity" cols="100" rows="6" maxlength="100" required>{{ $property->heading->activity }}</textarea>
</div>
<div class="gap gap-small"></div>
<input class="btn btn-primary" type="submit" value="Update" />
{!! Form::close() !!}
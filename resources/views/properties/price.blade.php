<h3>
<strong>Price</strong></h3>
<div>
    Rate : {{ $property->currency }} {{ $property->rate }}
</div>
<div>
    Rate Per Extra Person : {{ $property->currency }} {{ $property->rate_extra_person }}
</div>
<div>
    Cleaning Charges : {{ $property->currency }} {{ $property->rate_cleaning }}
</div>
<div>
    Weekly Discount : {{ $property->currency }} {{ $property->weekly_discount }}
</div>
<div>
    Monthly Discount : {{ $property->currency }} {{ $property->monthly_discount }}
</div>

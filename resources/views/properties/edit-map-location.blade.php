{{--<script src="https://maps.googleapis.com/maps/api/js"></script>--}}
<div id="map_canvas" style="height:400px;top:30px;width:700px;"></div>
<hr/>
<div>
    {!! Form::open(['url'=>'/property/map/'.$property->id.'/edit']) !!}
    <input id="address" type="hidden"  name="address" value="{{ $property->address->address.','.$property->address->city.','.$property->address->state.','.$property->address->country }}">
    <input type="hidden" id="lat" name="lat"/>
    <input type="hidden" id="lng" name="lng"/>
    <input type="hidden" name="rental_id" value="{{ $property->id }}">
    <input type="submit" name="submit" class="btn btn-primary" value="Save">
    {!! Form::close() !!}
</div>
<script>
    console.log('ready');
    var geocoder;
    var map;
    var mapOptions = {
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var marker;
    window.onload(initialize());
    function initialize() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        codeAddress();
    }
    function codeAddress() {
        var address = document.getElementById('address').value;
        console.log(address);
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                if(marker)
                    marker.setMap(null);
                marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    draggable: true
                });
                google.maps.event.addListener(marker, "dragend", function() {
                    document.getElementById('lat').value = marker.getPosition().lat();
                    document.getElementById('lng').value = marker.getPosition().lng();
                });
                document.getElementById('lat').value = marker.getPosition().lat();
                document.getElementById('lng').value = marker.getPosition().lng();
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
</script>
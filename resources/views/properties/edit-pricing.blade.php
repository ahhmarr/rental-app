{!! Form::open(['url' => 'property/pricing/'.$property->id.'/edit']) !!}
<div class="form-group form-group-lg form-group-select-plus col-md-3">
    <label for="currency">Currency:</label>
    <select class="form-control" name="currency" id="currency" required>
        <option value="{{ $property->currency }}">{{ $property->currency }}</option>
        <option value="">--------</option>
        <option value="$">USD</option>
        <option value="₹">INR</option>
        <option value="€">EURO</option>
    </select>
</div>
<div class="gap gap-small"></div>
<div class="form-group form-group-lg">
    <label for="rate">Base Price Per Night:</label>
    <input class="form-control" type="text" name="rate" id="rate" value="{{ $property->rate }}" required />
</div>
<div class="form-group form-group-lg">
    <label for="rate_extra_person">Rate per Extra Person:</label>
    <input class="form-control" type="text" name="rate_extra_person" id="rate_extra_person" value="{{ $property->rate_extra_person }}"  required />
</div>
<div class="form-group form-group-lg">
    <label for="rate_cleaning">Cleaning Charges:</label>
    <input class="form-control" type="text" name="rate_cleaning" id="rate_cleaning" value="{{ $property->rate_cleaning }}" required />
</div>
<h5><strong>Long Stay Price:</strong></h5><p>You might want to offer discount for longer stay</p>
<div class="form-group form-group-lg">
    <label for="weekly_discount">Weekly Discount Percent (%):</label>
    <input class="form-control" type="text" name="weekly_discount" id="weekly_discount"  value="{{ $property->weekly_discount }}" />
</div>
<div class="form-group form-group-lg">
    <label for="monthly_discount">Monthly Discount Percent (%):</label>
    <input class="form-control" type="text" name="monthly_discount" id="monthly_discount"  value="{{ $property->monthly_discount }}"  />
</div>
<div class="gap gap-small"></div>
<input class="btn btn-primary" type="submit" value="Update" />
{!! Form::close() !!}
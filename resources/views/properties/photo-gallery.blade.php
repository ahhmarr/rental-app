<div class="tab-pane fade in active" id="tab-1">
    <div class="fotorama" data-allowfullscreen="true" data-nav="thumbs">
        @if($property->photos)
        @foreach($property->photos as $image)
            <img src="{{ asset($image->rental_photo_url) }}" alt="Rental Rooms" />
            @endforeach
        @endif
    </div>
</div>
{!! Form::open(['url' => 'property/address/'.$property->id.'/edit']) !!}
<div id="locationField">
    <label for="autocomplete">Search Address</label>
    <input id="autocomplete" class="form-control" name="search_address" placeholder="Enter your address" onFocus="geolocate()" type="text" required />
</div>
<div class="form-group form-group-lg">
    <label for="street_number">Street Number</label>
    <input class="field form-control" name="street_number" id="street_number" disabled="true" value="{{ $property->address->street_number }}" />
</div>
<div class="form-group form-group-lg">
    <label for="route">Address</label>
    <input class="field form-control" name="address" id="route" disabled="true" value="{{ $property->address->address }}" required>
</div>
<div class="form-group form-group-lg">
    <label for="locality">City</label>
    <input class="field form-control" name="city" id="locality" disabled="true" value="{{ $property->address->city }}" required>
</div>
<div class="form-group form-group-lg">
    <label for="administrative_area_level_1">State</label>
    <input class="field form-control" name="state" id="administrative_area_level_1" disabled="true" value="{{ $property->address->state }}"/>
</div>
<div class="form-group form-group-lg">
    <label for="country">Country</label>
    <input class="field form-control" name="country" id="country" disabled="true" value="{{ $property->address->country }}" required />
</div>
<div class="form-group form-group-lg">
    <label for="postal_code">ZIP</label>
    <input class="field form-control" name="zip" id="postal_code" disabled="true" value="{{ $property->address->country }}" required />
</div>
<div class="form-group form-group-lg">
    <label for="phone_number">Phone Number</label>
    <input class="form-control" type="text" name="phone_number" id="phone_number"  placeholder="Phone Number" value="{{ $property->address->phone_number }}" required />
</div>
<div class="gap gap-small"></div>
<input class="btn btn-primary" type="submit" value="Update" />
{!! Form::close() !!}
{!! Html::script('js/map.js') !!}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1nTKTEbC2EsR3fm9qnJ1_LD4QqmKG-WQ&signed_in=true&libraries=places&callback=initAutocomplete" async defer></script>

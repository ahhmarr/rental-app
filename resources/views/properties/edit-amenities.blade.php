{!! Form::open(['url' => 'property/amenities/'.$property->id.'/edit']) !!}
<h5><strong>Common Amenities</strong></h5>
@foreach($common_amenities as $common_amenity)
    <div class="checkbox">
        <label><input class="i-check" type="checkbox" name="amenities_id[]" id="commonamenity{{ $common_amenity->id }}" value="{{ $common_amenity->id }}" />{{ $common_amenity->amenity }}</label>
    </div>
@endforeach
<hr/>
<h5><strong>Additional Amenities</strong></h5>
@foreach($additional_amenities as $additional_amenity)
    <div class="checkbox">
        <label><input class="i-check" type="checkbox" name="amenities_id[]" id="amenity{{ $additional_amenity->id }}" value="{{ $additional_amenity->id }}" />{{ $additional_amenity->amenity }}</label>
    </div>
@endforeach
<hr/>
<h5><strong>Special Features</strong></h5>
@foreach($special_amenities as $special_amenity)
    <div class="checkbox">
        <label><input class="i-check" type="checkbox" name="amenities_id[]" id="specialamenity{{ $special_amenity->id }}" value="{{ $special_amenity->id }}" />{{ $special_amenity->amenity }}</label>
    </div>
@endforeach
<hr/>
<p><a name="safety-features"></a></p>
<h5><strong>House Safeties</strong></h5>
@foreach($home_safeties_types as $home_safety)
    <div class="checkbox">
        <label><input class="i-check" type="checkbox" name="safeties_id[]" id="homeSafety{{ $home_safety->id }}" value="{{ $home_safety->id }}" />{{ $home_safety->safety }}</label>
    </div>
@endforeach
<hr/>
<h5><strong>House Rules</strong></h5>
@foreach($house_rules as $house_rule)
    <div class="checkbox">
        <label><input class="i-check" type="checkbox" name="amenities_id[]" id="homeSafety{{ $house_rule->id }}" value="{{ $house_rule->id }}" />{{ $house_rule->amenity }}</label>
    </div>
@endforeach
<div class="gap gap-small"></div>
<input class="btn btn-primary" type="submit" value="Update" />
{!! Form::close() !!}
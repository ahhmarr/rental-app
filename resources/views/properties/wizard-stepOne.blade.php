<!-- New property Wizard: Step One : User Registration -->
<ol class="progtrckr" data-progtrckr-steps="5">
    <li class="progtrckr-current">Step 1: Create Accout</li>
    <li class="progtrckr-todo">Step 2: List House</li>
    <li class="progtrckr-todo">Step 3: Get Published</li>
</ol>
<div class="col-md-4">
    <h3>Register</h3>
    @if ($errors->has('email'))
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            {{ $errors->first('email') }}
        </div>
    @endif
    @if ($errors->has('name'))
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            {{ $errors->first('name') }}
        </div>
    @endif
    @if ($errors->has('password'))
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            {{ $errors->first('password') }}
        </div>
    @endif
    {!! Form::open(['url'=>'new-property']) !!}
    {!! Form::hidden('type','owner') !!}
    <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon input-icon-show"></i>
        <label>Full Name</label>
        <input class="form-control" placeholder="e.g. John Doe" type="text" name="name" id="name" required />
    </div>
    <div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon input-icon-show"></i>
        <label>Email</label>
        <input class="form-control" placeholder="e.g. johndoe@gmail.com" type="email" name="email" id="email" required />
    </div>
    <div class="form-group form-group-icon-left"><i class="fa fa-lock input-icon input-icon-show"></i>
        <label>Password</label>
        <input class="form-control" type="password" placeholder="my secret password" name="password" id="password" required />
    </div>
    <div class="form-group form-group-icon-left"><i class="fa fa-lock input-icon input-icon-show"></i>
        <label>Re-type Password</label>
        <input class="form-control" type="password" placeholder="my secret password" name="password_confirmation" id="password_confirmation" required />
    </div>
    <input class="btn btn-primary" type="submit" value="Sign up" />
    {!! Form::close() !!}
</div>
<!-- New property Wizard Step One Close -->
{{----}}
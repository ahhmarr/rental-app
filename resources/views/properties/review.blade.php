<h3 class="mb20">Property Reviews</h3>
<div class="row row-wrap">
    <div class="col-md-8">
        {{--User Reviews --}}
        <ul class="booking-item-reviews list">
            @foreach($property->comments as $comment)
                <li>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="booking-item-review-person">
                                <a class="booking-item-review-person-avatar round" href="#">
                                    <img src="{{ $comment->user->userDetail->profile_pic }}" alt="Image Alternative text" title="Bubbles" />
                                </a>
                                <p class="booking-item-review-person-name"><a href="#">
                                        {{ $comment->user->name }}
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="booking-item-review-content">
                                <h5>"{{ $comment->title }}"</h5>
                                <ul class="icon-group booking-item-rating-stars">
                                    @for($i = 1; $i<= $comment->rating_average; $i++)
                                    <li>
                                        <i class="fa fa-star"></i>
                                    </li>
                                    @endfor
                                </ul>
                                <p>
                                    {{ $comment->text }}
                                </p>
                                <p class="text-small mt20">{{ $comment->created_at->diffForHumans() }}</p>
                                <div class="booking-item-review-more-content">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <ul class="list booking-item-raiting-summary-list">
                                                <li>
                                                    <div class="booking-item-raiting-list-title">Sleep</div>
                                                    <ul class="icon-group booking-item-rating-stars">
                                                        {{--maximum possible rating is 5.
                                                            number of yellow faces  = $comment->rating
                                                             number of white faces = 5 - $comment->rating --}}
                                                        @for($i = 1; $i<= $comment->rating_sleep; $i++)
                                                        <li>
                                                            <i class="fa fa-smile-o"></i>
                                                        </li>
                                                        @endfor
                                                        @for($i = 1; $i<= (5 - $comment->rating_sleep); $i++)
                                                        <li>
                                                            <i class="fa fa-smile-o text-gray"></i>
                                                        </li>
                                                        @endfor
                                                    </ul>
                                                </li>
                                                <li>
                                                    <div class="booking-item-raiting-list-title">Location</div>
                                                    <ul class="icon-group booking-item-rating-stars">
                                                        {{--maximum possible rating is 5.
                                                            number of yellow faces  = $comment->rating
                                                             number of white faces = 5 - $comment->rating --}}
                                                        @for($i = 1; $i<= $comment->rating_location; $i++)
                                                            <li>
                                                                <i class="fa fa-smile-o"></i>
                                                            </li>
                                                        @endfor
                                                        @for($i = 1; $i<= (5 - $comment->rating_location); $i++)
                                                            <li>
                                                                <i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        @endfor
                                                    </ul>
                                                </li>
                                                <li>
                                                    <div class="booking-item-raiting-list-title">Service</div>
                                                    <ul class="icon-group booking-item-rating-stars">
                                                        {{--maximum possible rating is 5.
                                                            number of yellow faces  = $comment->rating
                                                             number of white faces = 5 - $comment->rating --}}
                                                        @for($i = 1; $i<= $comment->rating_service; $i++)
                                                            <li>
                                                                <i class="fa fa-smile-o"></i>
                                                            </li>
                                                        @endfor
                                                        @for($i = 1; $i<= (5 - $comment->rating_service); $i++)
                                                            <li>
                                                                <i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        @endfor
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4">
                                            <ul class="list booking-item-raiting-summary-list">
                                                <li>
                                                    <div class="booking-item-raiting-list-title">Clearness</div>
                                                    <ul class="icon-group booking-item-rating-stars">
                                                        {{--maximum possible rating is 5.
                                                            number of yellow faces  = $comment->rating
                                                             number of white faces = 5 - $comment->rating --}}
                                                        @for($i = 1; $i<= $comment->rating_clearness; $i++)
                                                            <li>
                                                                <i class="fa fa-smile-o"></i>
                                                            </li>
                                                        @endfor
                                                        @for($i = 1; $i<= (5 - $comment->rating_clearness); $i++)
                                                            <li>
                                                                <i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        @endfor
                                                    </ul>
                                                </li>
                                                <li>
                                                    <div class="booking-item-raiting-list-title">Rooms</div>
                                                    <ul class="icon-group booking-item-rating-stars">
                                                        {{--maximum possible rating is 5.
                                                            number of yellow faces  = $comment->rating
                                                             number of white faces = 5 - $comment->rating --}}
                                                        @for($i = 1; $i<= $comment->rating_room; $i++)
                                                            <li>
                                                                <i class="fa fa-smile-o"></i>
                                                            </li>
                                                        @endfor
                                                        @for($i = 1; $i<= (5 - $comment->rating_room); $i++)
                                                            <li>
                                                                <i class="fa fa-smile-o text-gray"></i>
                                                            </li>
                                                        @endfor
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="booking-item-review-expand"><span class="booking-item-review-expand-more">More <i class="fa fa-angle-down"></i></span><span class="booking-item-review-expand-less">Less <i class="fa fa-angle-up"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
        <div class="gap gap-small"></div>
        <div class="box bg-gray">
            <h3>Write a Review</h3>
            <form method="POST" action="{{ url('/property/'.$property->id.'/comment') }}" name="property-review" id="property-review">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Review Title</label>
                            <input class="form-control" name="title" type="text" />
                        </div>
                        <div class="form-group">
                            <label>Review Text</label>
                            <textarea class="form-control" name="text" rows="6"></textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <ul class="list booking-item-raiting-summary-list stats-list-select">
                            <li>
                                <div class="booking-item-raiting-list-title">Sleep</div>
                                <ul id="comment_sleep" class="icon-group booking-item-rating-stars">
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div class="booking-item-raiting-list-title">Location</div>
                                <ul id="comment_location" class="icon-group booking-item-rating-stars">
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div class="booking-item-raiting-list-title">Service</div>
                                <ul id="comment_service" class="icon-group booking-item-rating-stars">
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div class="booking-item-raiting-list-title">Clearness</div>
                                <ul id="comment_clearness" class="icon-group booking-item-rating-stars">
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div class="booking-item-raiting-list-title">Rooms</div>
                                <ul id="comment_rooms" class="icon-group booking-item-rating-stars">
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                    <li><i class="fa fa-smile-o"></i>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div>
                                    <input type="hidden" name="rating_sleep" id="data_comment_sleep"/><br>
                                    <input type="hidden" name="rating_location" id="data_comment_location"/><br>
                                    <input type="hidden" name="rating_service" id="data_comment_service"/><br>
                                    <input type="hidden" name="rating_clearness" id="data_comment_clearness"/><br>
                                    <input type="hidden" name="rating_room" id="data_comment_rooms"/><br>
                                </div>
                            </li>
                        </ul>
                        <input type="submit" class="btn btn-primary"  name="submit-review" value="Leave a Review" />
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-4">
        <h4>Properties Near</h4>
        <ul class="booking-list">
            <li><a href="{{ URL::to('/property/1') }}">
                <div class="booking-item booking-item-small">
                    <div class="row">
                        <div class="col-xs-4">
                            <img src="{{ asset('img/hotel_porto_bay_rio_internacional_rooftop_pool_800x600.jpg') }}" alt="Image Alternative text" title="hotel PORTO BAY RIO INTERNACIONAL rooftop pool" />
                        </div>
                        <div class="col-xs-5">
                            <h5 class="booking-item-title">Styish, Chic, Best of West Village</h5>
                            <ul class="icon-group booking-item-rating-stars">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-3"><span class="booking-item-price">$159</span>
                        </div>
                    </div>
                </div></a>
            </li>
            <li><a href="{{ URL::to('/property/6') }}">
                <div class="booking-item booking-item-small">
                    <div class="row">
                        <div class="col-xs-4">
                            <img src="{{ asset('img/hotel_porto_bay_serra_golf_suite2_800x600.jpg') }}" alt="Image Alternative text" title="hotel PORTO BAY SERRA GOLF suite2" />
                        </div>
                        <div class="col-xs-5">
                            <h5 class="booking-item-title">Luxury Studio in Manhattan NYC</h5>
                            <ul class="icon-group booking-item-rating-stars">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star-o"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-3"><span class="booking-item-price">$237</span>
                        </div>
                    </div>
                </div></a>
            </li>
            <li><a href="{{ URL::to('property/3') }}">
                <div class="booking-item booking-item-small">
                    <div class="row">
                        <div class="col-xs-4">
                            <img src="{{ asset('img/hotel_eden_mar_suite_800x600.jpg') }}" alt="Image Alternative text" title="hotel EDEN MAR suite" />
                        </div>
                        <div class="col-xs-5">
                            <h5 class="booking-item-title">Luxury Apartment Theatre District</h5>
                            <ul class="icon-group booking-item-rating-stars">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star-half-empty"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-3"><span class="booking-item-price">$414</span>
                        </div>
                    </div>
                </div></a>
            </li>
            <li><a href="{{ URL::to('property/4') }}">
                <div class="booking-item booking-item-small">
                    <div class="row">
                        <div class="col-xs-4">
                            <img src="{{ asset('img/hotel_porto_bay_rio_internacional_de_luxe_800x600.jpg') }}" alt="Image Alternative text" title="hotel PORTO BAY RIO INTERNACIONAL de luxe" />
                        </div>
                        <div class="col-xs-5">
                            <h5 class="booking-item-title">Manhattan Beautiful Loft Excellent Loc</h5>
                            <ul class="icon-group booking-item-rating-stars">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star-half-empty"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-3"><span class="booking-item-price">$183</span>
                        </div>
                    </div>
                </div></a>
            </li>
            <li><a href="{{ URL::to('property/5') }}">
                <div class="booking-item booking-item-small" >
                    <div class="row">
                        <div class="col-xs-4">
                            <img src="{{ asset('img/the_pool_800x600.jpg') }}" alt="Image Alternative text" title="The pool" />
                        </div>
                        <div class="col-xs-5">
                            <h5 class="booking-item-title">The Meatpacking Suites - Luxury Lofts, Hot Location</h5>
                            <ul class="icon-group booking-item-rating-stars">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star-half-empty"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-3"><span class="booking-item-price">$476</span>
                        </div>
                    </div>
                </div></a>
            </li>
        </ul>
    </div>
</div>
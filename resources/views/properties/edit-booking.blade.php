{!! Form::open(['url' => 'property/booking/'.$property->id.'/edit']) !!}
<div class="form-group form-group-lg">
    <label for="check_in_time">Check In Time:</label>
    <input class="form-control" type="text" name="check_in_time" id="check_in_time" value="{{ $property->booking->check_in_time }}" required />
</div>
<div class="form-group form-group-lg">
    <label for="check_out_time">Check Out Time:</label>
    <input class="form-control" type="text" name="check_out_time" id="check_out_time" value="{{ $property->booking->check_out_time }}" required />
</div>
<div class="form-group form-group-lg form-group-select-plus col-md-3">
    <label for="security_deposit">Security Deposit:</label>
    <select class="form-control" name="security_deposit" id="security_deposit" required>
        <option value="{{ $property->booking->security_deposit }}">{{ $property->booking->security_deposit }}%</option>
        <option value="">-------</option>
        <option value="10">10%</option>
        <option value="15">15%</option>
        <option value="20">20%</option>
        <option value="25">25%</option>
        <option value="30">30%</option>
        <option value="40">40%</option>
        <option value="50">50%</option>
        <option value="100">100%</option>
    </select>
</div>
<div class="gap gap-small"></div>

<h3><strong>Cancellation Policy</strong></h3>
<p>For more information on our cancellation policy, please <a href="{{ url('cancellation-policy') }}" target="_blank">click here</a></p>
<div class="form-group form-group-lg form-group-select-plus col-md-3">
    <label for="cancel_policy"></label>
    <select class="form-control" name="cancel_policy" id="cancel_policy" required>
        <option value="">Select</option>
        <option value="1">Flexible</option>
        <option value="2">Moderate</option>
        <option value="3">Strict</option>
        <option value="4">Super Strict 30 Days</option>
        <option value="5">Super Strict 60 Days</option>
        <option value="6">Long Term</option>
    </select>
</div>
<div class="gap gap-small"></div>
<input class="btn btn-primary" type="submit" value="Save & Proceed" />
{!! Form::close() !!}
<script type="text/javascript">
    $('#check_in_time').timepicker({
        template: false,
        showInputs: false,
        minuteStep: 5});
    $('#check_out_time').timepicker({
        template: false,
        showInputs: false,
        minuteStep: 5});
</script>
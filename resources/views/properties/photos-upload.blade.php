@extends('layouts.properties-master')
@section('content')
    <div class="container">
        <h1 class="page-title">New Rental</h1>
    </div>
    @if(isset($rental_id))
        <div class="container">
        <div class="row">
            <div class="col-md-3">
                <aside class="sidebar-left">
                    <ul class="nav nav-pills nav-stacked nav-side mb30">
                        <li><a href="#">Upload Rental Photos</a></li>
                        <li><a href="#">Locate property on map</a></li>
                    </ul>
                </aside>
            </div>
            <div class="col-md-9">
                <script src="https://maps.googleapis.com/maps/api/js"></script>
                <h3>Upload Photos of Your House/Rooms</h3>
                {!! Html::script(asset('js/dropzone.js')) !!}
                {!! Form::open(array('url'=>'/property/new/photos','class'=>'dropzone', 'id'=>'my-awesome-dropzone')) !!}
                <input type="hidden" name="rental_id" value="{{ $rental_id }}">
                {!! Form::close() !!}
                <div class="gap gap-small"></div>
                <h3>Locate your house/apartment on this map</h3>
                <p>set your property location on this map so that your guest have no problem to reach you.</p>
                <div id="map_canvas" style="height:400px;top:30px;width:700px;"></div>
                <hr/>
                <div>
                    {!! Form::open(['url'=>'/property/new/map']) !!}
                    <input id="address" type="hidden"  name="address" value="{{ $address }}">
                    <input type="hidden" id="lat" name="lat"/>
                    <input type="hidden" id="lng" name="lng"/>
                    <input type="hidden" name="rental_id" value="{{ $rental_id }}">
                    <input type="submit" name="submit" class="btn btn-primary" value="Save">
                    {!! Form::close() !!}
                </div>
            </div>
            <hr/>
        </div>
    </div>
        <script>
            console.log('ready');
            var geocoder;
            var map;
            var mapOptions = {
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var marker;
            window.onload(initialize());
            function initialize() {
                geocoder = new google.maps.Geocoder();
                map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                codeAddress();
            }
            function codeAddress() {
                var address = document.getElementById('address').value;
                console.log(address);
                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        if(marker)
                            marker.setMap(null);
                        marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location,
                            draggable: true
                        });
                        google.maps.event.addListener(marker, "dragend", function() {
                            document.getElementById('lat').value = marker.getPosition().lat();
                            document.getElementById('lng').value = marker.getPosition().lng();
                        });
                        document.getElementById('lat').value = marker.getPosition().lat();
                        document.getElementById('lng').value = marker.getPosition().lng();
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
        </script>
    @endif
    @stop
@extends('layouts.properties-master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if($errors ->any())
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            @if(isset($stepOne))
                @include('properties.wizard-stepOne')
            @endif
            @if(isset($stepTwo))
                @include('properties.wizard-stepTwo')
            @endif
            @if(isset($stepThree))
                @include('properties.wizard-stepThree')
                @endif
        </div>
    </div>
</div>
@stop
<div>
    <h3><strong>Description</strong></h3>
    <div>
        <span>Home Type :<strong> {{ $property->homeType->home_type }}</strong></span>
    </div>
    <div>
        <span>Room Type :<strong> {{ $property->roomType->room_type }}</strong></span>
    </div>
    <div>
        <span>Bedrooms :<strong> {{ $property->bedroom }}</strong></span>
    </div>
    <div>
        <span>Beds :<strong> {{ $property->bed }}</strong></span>
    </div>
    <div>
        <span>Bathrooms :<strong> {{ $property->bathroom }}</strong></span>
    </div>
    <div>
        <span>Accommodates :<strong> {{ $property->accommodate }}</strong></span>
    </div>
</div>
<hr/>
<div class="col-md-3">
    <strong>Common Amenities</strong>
    @if($property->amenities)
        <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
            @foreach($property->amenities as $amenity)
            @if($amenity->amenity && $amenity->amenity->amenity_type_id==1)
            <li>
                <i class="fa fa-star"></i>
                <span class="booking-item-feature-title">
                    {{ $amenity->amenity->amenity }}
                </span>
                </li>
            @endif
           @endforeach
        </ul>
        @endif
    <strong>Additional Amenities</strong>
    @if($property->amenities)
    <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
        @foreach($property->amenities as $amenity)
            @if($amenity->amenity && $amenity->amenity->amenity_type_id==2)
        <li><i class="fa fa-star"></i><span class="booking-item-feature-title">{{ $amenity->amenity->amenity }}</span></li>
            @endif
        @endforeach
    </ul>
        @endif
    <strong>Special Amenities</strong>
    @if($property->amenities)
        <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
            @foreach($property->amenities as $amenity)
            @if($amenity->amenity && $amenity->amenity->amenity_type_id==3)
            <li>
                <i class="fa fa-star"></i>
                <span class="booking-item-feature-title">
                    {{ $amenity->amenity->amenity }}
                </span>
                </li>
            @endif
           @endforeach
        </ul>
        @endif
</div>
<div class="col-md-3">
    <strong>Safety Features</strong>
    @if($property->safeties)
    <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
          @foreach($property->safeties as $safety)
            @if($safety->safety)
            <li>
            <i class="fa fa-star"></i>
            <span class="booking-item-feature-title">{{ ($safety->safety->safety) }}
            </span>
            </li>
            @endif
            @endforeach
            @endif
    </ul>
    <strong>House Rules</strong>
    @if($property->amenities)
        <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
            @foreach($property->amenities as $amenity)
                @if($amenity->amenity && $amenity->amenity->amenity_type_id==4)
                    <li>
                        <i class="fa fa-star"></i>
                <span class="booking-item-feature-title">
                    {{ $amenity->amenity->amenity }}
                </span>
                    </li>
                @endif
            @endforeach
        </ul>
    @endif
</div>
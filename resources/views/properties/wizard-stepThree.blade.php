<!-- New property Wizard : Step Four : Upload Photos and Map location -->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<ol class="progtrckr" data-progtrckr-steps="5">
    <li class="progtrckr-done">Step 1: Account Created</li>
    <li class="progtrckr-current">Step 2: House List, Almost Done!</li>
    <li class="progtrckr-todo">Step 3: Get Published</li>
</ol>
<h3>Upload Photos of Your House/Rooms</h3>
{!! Html::script(asset('js/dropzone.js')) !!}
{!! Form::open(array('url'=>'new-property/photos','class'=>'dropzone', 'id'=>'my-awesome-dropzone')) !!}
<input type="hidden" name="rental_id" value="{{ $rental_id }}">
{!! Form::close() !!}
<div>
    <br>
    <hr/>
    <h3><strong>Locate your house/apartment on this map</strong></h3>
    <p>set your property location on this map so that your guest have no problem to reach you.</p>
    <div id="map_canvas" style="height:400px;top:30px;width:700px;"></div>
    <hr/>
    <div>
        {!! Form::open(['url'=>'new-property/map']) !!}
        <input id="address" type="hidden"  name="address" value="{{ $address }}">
        <input type="hidden" id="lat" name="lat"/>
        <input type="hidden" id="lng" name="lng"/>
        <input type="hidden" name="rental_id" value="{{ $rental_id }}">
        <input type="submit" name="submit" class="btn btn-primary" value="Get Published !">
        {!! Form::close() !!}
    </div>
    <hr/>
</div>
<script>
    var geocoder;
    var map;
    var mapOptions = {
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var marker;
    window.onload(initialize());
    function initialize() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        codeAddress();
    }
    function codeAddress() {
        var address = document.getElementById('address').value;
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                if(marker)
                    marker.setMap(null);
                marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    draggable: true
                });
                google.maps.event.addListener(marker, "dragend", function() {
                    document.getElementById('lat').value = marker.getPosition().lat();
                    document.getElementById('lng').value = marker.getPosition().lng();
                });
                document.getElementById('lat').value = marker.getPosition().lat();
                document.getElementById('lng').value = marker.getPosition().lng();
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
</script>
<!-- New property Wizard : Step Four : Close -->
@extends('layouts.properties-master')
@section('content')
<div class="container">
    <h1 class="page-title">Property Edit</h1>
</div>
<div class="container">
<div class="row">
    <div class="col-md-3">
        <aside class="sidebar-left">
            <ul class="nav nav-pills nav-stacked nav-side mb30">
                <li><a href="#basic-details" data-toggle="tab">Basic Details</a></li>
                <li><a href="#heading" data-toggle="tab">Heading</a></li>
                <li><a href="#location" data-toggle="tab">Address</a></li>
                <li><a href="#map-location" data-toggle="tab">Map Location</a></li>
                <li><a href="#amenities" data-toggle="tab">Amenities</a></li>
                <li><a href="#pricing" data-toggle="tab">Pricing</a></li>
                <li><a href="#bookings" data-toggle="tab">Booking</a></li>
                <li><a href="#photos" data-toggle="tab">Photos</a></li>
            </ul>
        </aside>
    </div>
    <div class="col-md-9">
        <h4>Select option from side bar to edit property</h4>
        <div class="tab-content">
            <div class="tab-pane" id="basic-details">
                @include('properties.edit-basic-details')
            </div>
            <div class="tab-pane" id="heading">
                @include('properties.edit-headings')
            </div>
            <div class="tab-pane" id="location">
                @include('properties.edit-address')
            </div>
            <div class="tab-pane" id="map-location">
                @include('properties.edit-map-location')
            </div>
            <div class="tab-pane" id="amenities">
                @include('properties.edit-amenities')
            </div>
            <div class="tab-pane" id="pricing">
                @include('properties.edit-pricing')
            </div>
            <div class="tab-pane" id="bookings">
                @include('properties.edit-booking')
            </div>
            <div class="tab-pane" id="photos">
                @include('properties.edit-photo')
            </div>
        </div>
    </div>
</div>
</div>
@stop
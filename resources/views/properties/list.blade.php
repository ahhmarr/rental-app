@extends('layouts.properties-master')
@section('content')
<div class="container">
    <h1 class="page-title">Rentals</h1>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-9">
            <div class="nav-drop booking-sort">
                <h5 class="booking-sort-title"><a href="#"><i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></h5>
                <ul class="nav-drop-menu">
                    <li><a href="#">Price (hight to low)</a>
                </li>
                <li><a href="#">Ranking</a>
            </li>
            <li><a href="#">Bedrooms (Most to Least)</a>
        </li>
        <li><a href="#">Bedrooms (Least to Most)</a>
    </li>
    <li><a href="#">Number of Reviews</a>
</li>
<li><a href="#">Number of Photos</a>
</li>
<li><a href="#">Just Added</a>
</li>
</ul>
</div>
@if(isset($properties) && !$properties->isEmpty())
<ul class="booking-list">
@foreach($properties as $property)
<li>
<div class="booking-item">
<div class="row">
<div class="col-md-3">
    <div class="booking-item-img-wrap">
        <img src="{{ asset($property->photos->first()->rental_photo_url) }}" alt="Image Alternative text" title="hotel PORTO BAY SERRA GOLF library" />
        <div class="booking-item-img-num"><i class="fa fa-picture-o"></i>9</div>
    </div>
</div>
<div class="col-md-6">
    <div class="booking-item-rating">
        <ul class="icon-group booking-item-rating-stars">
            <li><i class="fa fa-star"></i>
            </li>
            <li><i class="fa fa-star"></i>
            </li>
            <li><i class="fa fa-star"></i>
            </li>
            <li><i class="fa fa-star"></i>
            </li>
            <li><i class="fa fa-star-half-empty"></i>
            </li>
            </ul><span class="booking-item-rating-number"><b >4.6</b> of 5</span><small>(1329 reviews)</small>
        </div>
        <h5 class="booking-item-title">{{ $property->heading->heading }}</h5>
        <p class="booking-item-address"><i class="fa fa-map-marker"></i>{{ $property->address->search_address }}</p>
        <ul class="booking-item-features booking-item-features-rentals booking-item-features-sign">
            <li rel="tooltip" data-placement="top" title="Sleeps"><i class="fa fa-male"></i><span class="booking-item-feature-sign">{{ $property->accommodate }}</span></li>
            <li rel="tooltip" data-placement="top" title="Bedrooms"><i class="im im-bed"></i><span class="booking-item-feature-sign">{{ $property->bedroom }}</span></li>
            <li rel="tooltip" data-placement="top" title="Bathrooms"><i class="im im-shower"></i><span class="booking-item-feature-sign">{{ $property->bathroom }}</span></li>
        </ul>
    </div>
    <div class="col-md-3">
        <span class="booking-item-price">{{ $property->currency }}{{ $property->rate }}</span>
        <span>/night</span>
        @if($property->user->id==$user_id)
        <span><a href="{{ URL::to('property/'.$property->id.'/edit') }}" class="btn btn-primary">Edit</a></span>
        @endif
        <span><a href="{{ URL::to('property/'.$property->id) }}" class="btn btn-primary">View</a></span>
    </div>
</div>
</div>
</li>
@endforeach
</ul>
@else
<h3>You Do Not have any registered property</h3>
<a href="{{ url('property/new') }}" class="btn btn-primary btn-lg">Add New Property</a>
@endif
<div class="gap gap-big"></div>
<div class="row">
<div class="col-md-6">
<!-- <p><small>320 vacation rentals found in New York. &nbsp;&nbsp;Showing 1 – 15</small>
</p> -->
<ul class="pagination">
<li class="active"><a href="#">1</a></li>
<!-- <li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li><a href="#">5</a></li>
<li><a href="#">6</a></li>
<li><a href="#">7</a></li>
<li class="dots">...</li>
<li><a href="#">43</a></li> -->
<li class="next"><a href="#">Next Page</a></li>
</ul>
</div>
<div class="col-md-6 text-right">
<!-- <p>Not what you're looking for? <a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out">Try your search again</a>
</p> -->
</div>
</div>
</div>
</div>
<div class="gap"></div>
</div>
@stop
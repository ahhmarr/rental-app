@extends('layouts.properties-master')
@section('content')
    <div class="container">
        <h1 class="page-title">Tour Booking Requests</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php $x=0; ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Address</th>
                        <th>Rooms</th>
                        <th>Guests</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Message</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bookingRequests as $request)
                        <tr>
                            <th>{{ ++$x }}</th>
                            <th>{{ $request->bookingRequest->heading->heading }}</th>
                            <th>{{ $request->bookingRequest->address->city }},
                                {{ $request->bookingRequest->address->state }},
                                {{ $request->bookingRequest->address->country }}</th>
                            <th>{{ $request->room }}</th>
                            <th>{{ $request->guest }}</th>
                            <th>{{ $request->start->format('j-F \' y') }}</th>
                            <th>{{ $request->end->format('j-F \'y') }}</th>
                            <th><a href="{{ url('message') }}" class="btn btn-primary btn-sm">view</a></th>
                            <th><a href="{{ url('tour-request-delete/'.$request->id) }}" class="btn btn-primary btn-sm" >delete</a></th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                    @if($bookingRequests->isEmpty())
                            <span>No Data Found</span>
                    @endif
                <!-- booking request message modals -->
                @foreach($bookingRequests as $message)
                    <div class="modal fade" id="message{{ $message->id }}" tabindex="-1" role="dialog" aria-labelledby="message">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5>Message<button class="close" data-dismiss="modal">X</button></h5>
                                </div>
                                <div class="modal-body">
                                    {!! $message->message !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
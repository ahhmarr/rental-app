<div class="container">
    <div class="nav">
        <ul class="slimmenu" id="slimmenu">
            <li><a href="#">Home</a>
                <ul>
                    <li>
                        {!! Html::link('/','home')!!}
                    </li>
                    <li>
                        {!! Html::link('cancellation-policy','Cancellation')!!}
                    </li>
                </ul>
            <li><a href="#">Search</a>
                <ul>
                    <li><a href="{{ url('/search-all') }}">Rentals</a></li>
                </ul>
            </li>
            <li><a href="{{ url('new-property')}}" class="btn btn-primary btn-lg" role="button">Add Properties</a>
            </li>
        </ul>
    </div>
</div>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="RnxLeo">

    <title>Admin Dashboard</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('adminDashboardAssets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminDashboardAssets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('adminDashboardAssets/css/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('adminDashboardAssets/css/footer-distributed.css') }}">

    <script src="{{ asset('adminDashboardAssets/js/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('adminDashboardAssets/js/bootstrap.min.js') }}"></script>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top hidden-print" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Kingdom of Rentals</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('admin/') }}">Dashboard</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">List<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>{!! Html::linkAction('PropertyAttributesController@homeTypes','Home Types') !!}</li>
                        <li>{!! Html::linkAction('PropertyAttributesController@roomTypes','Room Types') !!}</li>
                        <li>{!! Html::linkAction('PropertyAttributesController@amenities','Amenities') !!}</li>
                        <li>{!! Html::linkAction('PropertyAttributesController@houseSafeties','House Safeties') !!}</li>
                    </ul>
                </li>
            </ul>
            <div class="col-sm-3 col-md-3 pull-right">
                <form class="navbar-form" role="search" name="search" method="GET" action="#">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="#" name="search">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
</nav>
@yield('content')
<footer class="footer-distributed hidden-print">
    <div class="footer-left">
        <div style="color: white;">Some Footer</div>
        <p>&copy; 2015</p>
    </div>

</footer>


</body>

</html>


<!DOCTYPE HTML>
<html>

<head>
    <title>Traveler - Index</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mystyles.css') }}">
    <script src="{{ asset('js/modernizr.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('css/switcher.css') }}" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/bright-turquoise.css') }}" title="bright-turquoise" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/turkish-rose.css') }}" title="turkish-rose" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/salem.css') }}" title="salem" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/hippie-blue.css') }}" title="hippie-blue" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/mandy.css') }}" title="mandy" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/green-smoke.css') }}" title="green-smoke" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/horizon.css') }}" title="horizon" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/cerise.css') }}" title="cerise" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/brick-red.css') }}" title="brick-red" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/de-york.css') }}" title="de-york" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/shamrock.css')}}" title="shamrock" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/studio.css')}}" title="studio" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/leather.css')}}" title="leather" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/denim.css')}}" title="denim" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/schemes/scarlet.css')}}" title="scarlet" media="all" />
</head>

<body>

<!-- FACEBOOK WIDGET -->
<div id="fb-root"></div>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- /FACEBOOK WIDGET -->
<div class="global-wrap">
    <header id="main-header">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <a class="logo" href="{{ url('/') }}">
                            <img src="{{ asset('img/logo-invert.png') }}" alt="Image Alternative text" title="Image Title" />
                        </a>
                    </div>
                    <div class="col-md-3 col-md-offset-2">
                        <form class="main-header-search">
                            <div class="form-group form-group-icon-left">
                                <i class="fa fa-search input-icon"></i>
                                <input type="text" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <div class="top-user-area clearfix">
                            @if (Auth::guest())
                                <ul class="top-user-area-list list list-horizontal list-border">
                                    <li><a href="{{ url('/login') }}">Login</a></li>
                                    <li><a href="{{ url('/register') }}">Register</a></li>
                                </ul>
                            @else
                                <ul class="top-user-area-list list list-horizontal list-border">
                                    <li class="top-user-area-avatar">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            <img class="origin round" src="{{ asset(Auth::user()->userDetail->profile_pic) }}" alt="Image Alternative text" title="AMaze" />{{ Auth::user()->name }}</a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                            <li><a href="{{ url('/profile/'.Auth::user()->id) }}"><i class="fa fa-btn fa-user"></i>Profile</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(Auth::check())
            @include('layouts.user-menu')
        @else
            @include('layouts.guest-menu')
        @endif
    </header>
@yield('content')
<div class="gap"></div>
    <footer id="main-footer">
        <div class="container">
            <div class="row row-wrap">
                <div class="col-md-3">
                    <a class="logo" href="#">
                        <img src="{{ asset('img/logo-invert.png') }}" alt="Image Alternative text" title="Image Title" />
                    </a>
                    <p class="mb20">Booking, reviews and advices on hotels, resorts, flights, vacation rentals, travel packages, and lots more!</p>
                    <ul class="list list-horizontal list-space">
                        <li>
                            <a class="fa fa-facebook box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-twitter box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-google-plus box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-linkedin box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-pinterest box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-3">
                    <h4>Newsletter</h4>
                    <form>
                        <label>Enter your E-mail Address</label>
                        <input type="text" class="form-control">
                        <p class="mt5"><small>*We Never Send Spam</small>
                        </p>
                        <input type="submit" class="btn btn-primary" value="Subscribe">
                    </form>
                </div>
                <div class="col-md-2">
                    <ul class="list list-footer">
                        <li><a href="#">About US</a>
                        </li>
                        <li><a href="#">Press Centre</a>
                        </li>
                        <li><a href="#">Best Price Guarantee</a>
                        </li>
                        <li><a href="#">Travel News</a>
                        </li>
                        <li><a href="#">Jobs</a>
                        </li>
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                        <li><a href="#">Feedback</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h4>Have Questions?</h4>
                    <h4 class="text-color">+1-202-555-0173</h4>
                    <h4><a href="#" class="text-color">support@traveler.com</a></h4>
                    <p>24/7 Dedicated Customer Support</p>
                </div>

            </div>
        </div>
    </footer>

    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/slimmenu.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('js/nicescroll.js') }}"></script>
    <script src="{{ asset('js/dropit.js') }}"></script>
    <script src="{{ asset('js/ionrangeslider.js') }}"></script>
    <script src="{{ asset('js/icheck.js') }}"></script>
    <script src="{{ asset('js/fotorama.js') }}"></script>
    <script src="{{ asset('js/typeahead.js') }}"></script>
    <script src="{{ asset('js/card-payment.js') }}"></script>
    <script src="{{ asset('js/magnific.js') }}"></script>
    <script src="{{ asset('js/owl-carousel.js') }}"></script>
    <script src="{{ asset('js/fitvids.js') }}"></script>
    <script src="{{ asset('js/tweet.js') }}"></script>
    <script src="{{ asset('js/countdown.js') }}"></script>
    <script src="{{ asset('js/gridrotator.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/switcher.js') }}"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="{{ asset('js/jquery.geocomplete.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#address').geocomplete();
        });
    </script>
</div>
</body>
</html>
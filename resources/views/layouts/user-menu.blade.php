<div class="container">
    <div class="nav">
        <ul class="slimmenu" id="slimmenu">
            <li><a href="#">Home</a>
                <ul>
                    <li>
                        {!! Html::link('/','home')!!}
                    </li>
                    <li>
                        {!! Html::link('cancellation-policy','Cancellation')!!}
                    </li>
                </ul>
            <li><a href="#">My Property</a>
                <ul>
                    <li><a href="{{ url('/property/new')}}">Add New</a></li>
                    <li><a href="{{ url('property-list') }}">List All</a></li>
                    <li><a href="{{ url('booking-request') }}">Requests</a></li>
                    <li><a href="{{ url('booking') }}">Bookings</a></li>
                </ul>
            </li>
            <li><a href="#">Tour</a>
                <ul>
                    <li><a href="{{ url('tour-request') }}">Requests</a> </li>
                    <li><a href="{{ url('tour-booking') }}">Bookings</a> </li>
                </ul>
            </li>
            <li><a href="#">Profile</a>
                <ul>
                    @if(Auth::check())
                        <li><a href="{{ url('profile/'.Auth::user()->id) }}">View</a></li>
                        <li><a href="{{ url('profile/edit') }}">Edit</a></li>
                    @endif
                </ul>
            </li>
            <li><a href="#">Message</a>
                <ul>
                    <li><a href="{{ url('message') }}">Inbox</a></li>
                </ul>
            </li>
            <li><a href="#">Search</a>
                <ul>
                    <li><a href="{{ url('/search-all') }}">Rentals</a></li>
                </ul>
            </li>
            <li><a href="">News <span class="badge">
                        {{ Auth::user()->notifications()->unread()->count() }}
                    </span></a>
                <ul>
                    <li><a href="{{ url('/notification') }}">Notifications<span class="badge">
                                {{ Auth::user()->notifications()->unread()->count() }}
                            </span></a></li>
                </ul>
            </li>
        </ul>

    </div>
</div>
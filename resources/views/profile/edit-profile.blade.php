@extends('layouts.properties-master')
@section('content')
    <div class="container">
        <h1 class="page-title">Edit Profile</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <aside class="user-profile-sidebar">
                    <div class="user-profile-avatar text-center">
                        <img src="{{ asset($profile->profile_pic) }}" alt="Image Alternative text" title="AMaze" />
                        <h5>{{ $profile->users->name }}</h5>
                        <p>Member Since {{ $profile->users->created_at->toFormattedDateString() }}</p>
                    </div>
                    <ul class="list user-profile-nav">
                        <li><a href="{{ url('profile/'.$profile->users->id) }}"><i class="fa fa-user"></i>Overview</a>
                        </li>
                        <li><a href="{{ url('profile/edit') }}"><i class="fa fa-cog"></i>Settings</a>
                        </li>
                    </ul>
                </aside>
                <aside class="sidebar-left">
                    <p><strong>Upload Photo</strong></p>
                    {!! Html::script(asset('js/dropzone.js')) !!}
                    {!! Form::open(array('url'=>'profile/edit-pic','class'=>'dropzone', 'id'=>'my-awesome-dropzone')) !!}
                    {!! Form::close() !!}
                </aside>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-5">
                        {!! Form::open(['method'=>'PATCH','url'=>'profile/edit']) !!}
                            <h4>Personal Infomation</h4>
                            <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                <label>Name</label>
                                <input class="form-control" name="name" value="{{ $profile->users->name }}" type="text" required />
                            </div>
                            <div class="form-group form-group-icon-left"><i class="fa fa-phone input-icon"></i>
                                <label>Phone Number</label>
                                <input class="form-control" name="phone_number" value="{{ $profile->phone_number }}" type="text" required />
                            </div>
                            <div class="gap gap-small"></div>
                            <h4>Location</h4>
                            <div class="form-group form-group-icon-left">
                                <label>Street</label>
                                <input class="form-control" name="street" value="{{ $profile->street }}" type="text" />
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input class="form-control" name="address" value="{{ $profile->address }}" type="text" />
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <input class="form-control" name="city" value="{{ $profile->city }}" type="text" required />
                            </div>
                            <div class="form-group">
                                <label>State/Province/Region</label>
                                <input class="form-control" name="state" value="{{ $profile->state }}" type="text" required />
                            </div>
                            <div class="form-group">
                                <label>ZIP code/Postal code</label>
                                <input class="form-control" name="zip" value="{{ $profile->zip }}" type="text" required />
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <input class="form-control" name="country" value="{{ $profile->country }}" type="text" required />
                            </div>
                            <div class="gap gap-small"></div>
                            <h4>About Me /Work /Hobbies</h4>
                            <div class="form-group">
                                <label>About Me</label>
                                <textarea name="about" class="form-control" required>{{ $profile->about }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Work</label>
                                <textarea name="work" class="form-control">{{ $profile->work }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Hobbies /Interests</label>
                                <textarea name="hobbies" class="form-control">{{ $profile->hobbies }}</textarea>
                            </div>
                            <hr>
                            <input type="submit" class="btn btn-primary" value="Save Changes">
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <h4>Change Password</h4>
                        <form>
                            <div class="form-group form-group-icon-left"><i class="fa fa-lock input-icon"></i>
                                <label>Current Password</label>
                                <input class="form-control" type="password" />
                            </div>
                            <div class="form-group form-group-icon-left"><i class="fa fa-lock input-icon"></i>
                                <label>New Password</label>
                                <input class="form-control" type="password" />
                            </div>
                            <div class="form-group form-group-icon-left"><i class="fa fa-lock input-icon"></i>
                                <label>New Password Again</label>
                                <input class="form-control" type="password" />
                            </div>
                            <hr />
                            <input class="btn btn-primary" type="submit" value="Change Password" />
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
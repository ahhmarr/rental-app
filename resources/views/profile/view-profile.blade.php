@extends('layouts.properties-master')
@section('content')
    <div class="container">
        <h1 class="page-title">Profile</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <aside class="user-profile-sidebar">
                    <div class="user-profile-avatar text-center">
                        <img src="{{ asset($profile->profile_pic) }}" alt="Image Alternative text" title="AMaze" />
                        <h5>{{ $profile->users->name }}</h5>
                        <p>Member Since {{ $profile->users->created_at->toFormattedDateString() }}</p>
                    </div>
                    <ul class="list user-profile-nav">
                        <li><a href="{{ url('profile/'.$profile->users->id) }}"><i class="fa fa-user"></i>Overview</a>
                        </li>
                        <li><a href="{{ url('profile/edit') }}"><i class="fa fa-cog"></i>Settings</a>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-5">
                            <h4>Personal Infomation</h4>
                            <div>
                                <span>Name : </span><h5><strong>{{ $profile->users->name }}</strong></h5>
                            </div>
                            <div>
                                <span>Email : </span><h5><strong>{{ $profile->users->email }}</strong></h5>
                            </div>
                            <div>
                                <span>Phone : </span><h5><strong>{{ $profile->phone_number }}</strong></h5>
                            </div>
                            <div class="gap gap-small"></div>
                            <h4>Location</h4>
                            <div>
                                <span>Address : </span><h5><strong>
                                        {{ $profile->street }} ,
                                        {{ $profile->address }},
                                        {{ $profile->city}}, {{  $profile->state}} ,
                                        {{  $profile->country}} -
                                        {{ $profile->zip }}</strong></h5>
                            </div>
                            <div>
                                <span>City : </span><h5><strong>{{ $profile->city }}</strong></h5>
                            </div>
                            <div>
                                <span>State/Province/Region : </span><h5><strong>{{ $profile->state }}</strong></h5>
                            </div>
                            <div>
                                <span>Zip code/Postal code : </span><h5><strong>{{ $profile->zip }}</strong></h5>
                            </div>
                            <div>
                                <span>Country : </span><h5><strong>{{ $profile->country }}</strong></h5>
                            </div>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <h4>About /Work /Hobbies</h4>
                        <div>
                            <span><strong>About :</strong></span><h5>{{ $profile->about }}</h5>
                        </div>
                        <div>
                            <span><strong>Work :</strong></span><h5>{{ $profile->work }}</h5>
                        </div>
                        <div>
                            <span><strong>Hobbies /Interests :</strong></span><h5>{{ $profile->hobbies }}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
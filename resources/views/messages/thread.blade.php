<ul class="booking-item-reviews list"> {{-- Message List Starts --}}
    <li>{{-- Message starts --}}
        @foreach($message_threads as $thread)
        <div class="row">
            <a class="col-md-10" href="{{ url('message/'.$thread->id) }}">
                <div class="booking-item-review-content">
                    <p>
                        {!! $thread->title !!}

                    </p>
                </div>
            </a>
        </div>
        @endforeach
    </li> {{-- Message Ends --}}
</ul> {{-- Message List Ends --}}
<ul class="booking-item-reviews list"> {{-- Message List Starts --}}
    @foreach($chats as $chat)
    <li>{{-- Message starts --}}
        <div class="row">
            <div class="col-md-2">
                <div class="booking-item-review-person">
                    <a class="booking-item-review-person-avatar round" href="{{ url('profile/'.$chat->sender->id) }}">
                        <img src="{{ asset($chat->sender->userDetail->profile_pic) }}" alt="Image Alternative text" title="{{ $chat->sender->name }}" />
                    </a>
                </div>
            </div>
            <div class="col-md-10">
                <div class="booking-item-review-content">
                    <p>
                        @if($chat->message != '')
                            {!! $chat->message !!}
                            @endif
                        @if($chat->attachment == 1)
                            @if(count($chat->fileAttached) > 0)
                                    <span><strong>Attachments:</strong></span><br>
                            @endif
                            @foreach($chat->fileAttached as $file)
                                <a href="{{ url($file->link) }}" target="_blank">{{ $file->file }}</a>
                                <br>
                            @endforeach
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </li> {{-- Message Ends --}}
    @endforeach
</ul> {{-- Message List Ends --}}
<div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-10">
        <form class="form" method="POST" action="{{ url('message/'.$chat->thread_id.'/new-chat') }}" enctype="multipart/form-data">
        <div class="form-group">
            <label for="message"></label>
            <textarea  name="message" id="message" cols="30" rows="10"></textarea>
        </div>
            <div class="form-group">
                <label for="file_upload">Upload File</label>
                <input type="file" id="my_file" name="my_file[]" multiple class="form-control" style="padding-bottom: 5%;">
            </div>
        <button type="submit" class="btn btn-primary">Send</button>
        </form>
        <div class="gap gap-small"></div>
        @if($errors->any())
            <ul>
                @foreach($errors as $error)
                    <li>{{ $error }}</li>
                    @endforeach
            </ul>
            @endif
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#message').redactor();
    });
</script>
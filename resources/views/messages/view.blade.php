@extends('layouts.properties-master')
@section('content')
    <div class="container">
        <h1 class="page-title">Messages</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <aside class="sidebar-left">
                    <ul class="nav nav-pills nav-stacked nav-side mb30">
                        <li>
                            <a href="{{ url('message') }}">Inbox</a>
                        </li>
                    </ul>
                </aside>
                <div class="sidebar-left">
                    @if(isset($title))
                        <h5>{!! $title->title !!}</h5>
                    @if(isset($title->booking_id))
                    <h5><button href="{{ url('booking-confirm/'.$title->booking_id) }}" class="btn btn-success btn-lg">confirm Booking</button></h5>
                        @endif
                        <div class="gap gap-small"></div>
                    @endif
                </div>
            </div>
            <div class="col-md-9">
                @if(isset($message_threads))
                    @include('messages.thread')
                    @endif
                @if(isset($chats))
                    @include('messages.chat')
                    @endif
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.btn-success').click(function() {
                if (confirm('Confirm Booking, Are you sure?')) {
                    var url = $(this).attr('href');
                    location.href= url;
                }
            });
        });
    </script>
    @stop